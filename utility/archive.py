import discord
import logging

from redbot.core import Config, checks, commands, utils
from redbot.core.utils.mod import mass_purge
from copy import copy
from datetime import datetime, timedelta
from typing import List, Union

log = logging.getLogger("utility")

async def _archive_helper(ctx, archive_channel, to_delete:List[discord.Message]):
    channel = ctx.channel
    author = ctx.author
    message = ctx.message

    await _backup_msg(archive_channel, to_delete)

    log.info("Archivage de {} messages par {} dans {}".format(len(to_delete), author, channel))
    
    await mass_purge(to_delete, channel)

    try:
        await message.delete()
    except:
        pass

async def _backup_msg(archive_channel, messages):
    archive_message = "##### DEBUT Archivage #####\n\n"
    for msg in messages:
        header = f"<{msg.author}> [{msg.author.id}]:"
        tmp = f"{header}\n{msg.clean_content}\n\n"

        if len(archive_message) + len(tmp) >= 1950:
            await archive_channel.send(f"```{archive_message}```")
            archive_message = ""

            # 1950 because 2000 discord char limit minus 6 ` for formatting, rounded down to 1950 for margin of error
            # Header is max 38 + 18 + 6 = 62 characters
            #   38 = max msg.author length
            #   18 = msg.author.id
            #   6 = formatting 
            high_limit = (1950 - len(header))

            if high_limit <= len(msg.clean_content):
                # If msg content is almost at 2000 discord char limit
                await archive_channel.send(f"```{header}```")
                await archive_channel.send(f"{msg.clean_content}")
            else:
                archive_message += tmp
        else:
            archive_message += tmp

        if len(msg.attachments) >= 1:
            await archive_channel.send(f"```{archive_message}```")
            archive_message = ""

        for attachment in msg.attachments:
            file = await attachment.to_file()
            await archive_channel.send(file=file)
    
    await archive_channel.send(f"```{archive_message}#####  FIN Archivage  #####```")
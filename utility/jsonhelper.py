import json

class Serializable:
    def serialize(self):
        return self.__dict__
    
    @staticmethod
    def deserialize():
        pass

def save_json_objects(filename, list):
    with open(filename,"w") as f:
        json_objects = json.dumps([ob.serialize() for ob in list])
        f.write(json_objects)


def load_json_objects(filename,classname):
    objects = []
    with open(filename, 'r') as f:
        json_objects = json.load(f)
        for object in json_objects:
            objects.append(classname.deserialize(object))
    return objects


def load_json_dict(filename):
    dict = []
    with open(filename, 'r') as f:
        dict = json.load(f)
    return dict
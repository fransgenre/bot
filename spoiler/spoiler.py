import logging
import asyncio
from datetime import datetime, timedelta
from typing import List

import discord
from redbot.core import commands

log = logging.getLogger("red.autodelete")


class Spoiler(commands.Cog):

    def __init__(self, bot):
        super().__init__()
        self.bot = bot

    @commands.command(name="spoiler", aliases=['s'])
    async def spoiler(self, ctx: commands.context):
        """
        Spoil the image
        """
        files = []
        for attachment in ctx.message.attachments:
            file = await attachment.to_file(spoiler=True)
            file.filename = f"SPOILER_{attachment.filename}"
            files.append(file)

        message_content=""
        split_content = ctx.message.clean_content.split(' ', 1)
        if len(split_content) >= 2:
            for word in split_content[1].split(' '):
                if word.startswith("http"):
                    message_content += "||" + word + "|| "
                else:
                    message_content += word + " "
        
        message = ctx.author.display_name + ":\n" + message_content
        await ctx.send(content=message, files=files)
        await ctx.message.delete()


import discord
import logging
from redbot.core import Config, checks, commands, utils

log = logging.getLogger("red.test")

class Test(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20210523)
        default_guild = {}
        self.config.register_guild(**default_guild)

    @commands.command(name="get_message")
    @checks.admin_or_permissions(administrator = True)
    async def get_message(self, ctx, message: discord.Message):
        log.info(message.content)
        log.info(discord.utils.remove_markdown(message.content))
        log.info("*" in message.content)
        log.info("*" in discord.utils.remove_markdown(message.content))

    @commands.command()
    @checks.admin_or_permissions(administrator = True)
    async def test_message(self, ctx, message: discord.Message):
        await ctx.send(f"auteur: {message.author.id}, channel: {message.channel.id}, couleur: {message.embeds[0].colour.value}\nmessage de bump: {message.author.id == 302050872383242240 and message.channel.id == 820768365558759534 and message.embeds[0].colour.value == 0x24b7b7}")
import asyncio
import discord
import time

from datetime import date, datetime, timedelta
from redbot.core import Config, checks, commands
from redbot.core.utils.embed import randomize_colour
import logging

class Abort(Exception):
    pass

import importlib.util
spec = importlib.util.spec_from_file_location("cogs.utility", "/cogs/utility/jsonhelper.py")
jsonhelper = importlib.util.module_from_spec(spec)
spec.loader.exec_module(jsonhelper)

log = logging.getLogger("red.purge")

class PurgeData(jsonhelper.Serializable):
    def __init__(self, date, member_list):
        self.date = date
        self.member_list = member_list
        
    def deserialize(object):
        return PurgeData(datetime.fromisoformat(object['date']), object['member_list'])
    
    def serialize(self):
        return {
            'date':str(self.date),
            'member_list':self.member_list
        }

class Purge(commands.Cog):

    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20210223)
        default_guild = {
            "ignored_channels": [],
            "ignored_category": [],
            "ignored_roles": [],
            "days": 30,
            "kick_message": None
        }
        self.purge_history_filename = "/cogs/purge/purge.json"
        self.purgeHistory = jsonhelper.load_json_objects(self.purge_history_filename, PurgeData)
        self.config.register_guild(**default_guild)

################################################### COMMANDS

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def purge(self, ctx):
        """Clean up inactive members"""
        pass

    @purge.command(name = "list")
    async def purge_list(self, ctx):
        """List inactive members"""
        async with ctx.channel.typing():
            progress_message = await ctx.send("Initialisation...")
            today = datetime.combine(date.today(), datetime.min.time())
            configured_days = await self.config.guild(ctx.guild).days()
            delta_date = today - timedelta(days = configured_days)

            ignored_roles_ids = await self.config.guild(ctx.guild).ignored_roles()
            ignored_roles = sorted(list(map(lambda r: ctx.guild.get_role(r), ignored_roles_ids)), key = lambda r: r.position, reverse = True)
            ignored_channels = await self.config.guild(ctx.guild).ignored_channels()
            ignored_category = await self.config.guild(ctx.guild).ignored_category()

            active_members = set()
            for channel in [c for c in ctx.guild.text_channels if c.id not in ignored_channels and c.category_id not in ignored_category]:
                await progress_message.edit(content=f"Recherche des membres actifs. Salon: {channel}")
                async for message in channel.history(limit = None, after = delta_date):
                    active_members.add(message.author)

            inactive_members_count = 0
            inactive_members = []
            members_with_ignored_role = {}

            await progress_message.edit(content=f"Recherche des membres inactifs. {len(active_members)} membres actifs trouvés")

            for member in ctx.guild.members:
                    # On ignore les bots
                    if member.bot:
                        continue
                    
                    if member.joined_at > delta_date:
                        continue

                    # On ignore les membres actifs
                    if member in active_members:
                        continue

                    # On traite autrement le membre s'il a un rôle configuré comme ignoré
                    if any(r.id in ignored_roles_ids for r in member.roles):
                        for role in ignored_roles:
                            if role in member.roles:
                                if role in members_with_ignored_role:
                                    members_with_ignored_role[role].append(member)
                                else:
                                    members_with_ignored_role[role] = [member]
                                break
                    else:
                        inactive_members.append(member)
                    
                    inactive_members_count += 1
            
            await progress_message.delete()

            for purge in self.purgeHistory:
                already_purged = [m for m in inactive_members if m.id in purge.member_list]
                inactive_members = [m for m in inactive_members if m.id not in purge.member_list]
                
                await self.paginated_send(ctx, already_purged, f"⚠️ Ont déjà été purge le {purge.date}\n\n")


            await self.paginated_send(ctx, inactive_members)
            for role, members in members_with_ignored_role.items():
                await self.paginated_send(ctx, members, f"⚠️ Possèdent le rôle {role.name}\n\n")

            summary = "Nombre total de membres inactifs : {}\n".format(inactive_members_count)
            summary += "Nombre de membres inactifs sans role : {}\n".format(len(inactive_members))
            summary += "Nombre total de membres : {}".format(len([m for m in ctx.guild.members if not m.bot]))
            embed = discord.Embed(title = "Résumé", description = summary, color = 0xde2100)
            embed.set_footer(text = "Utilisez !purge_setup pour gérer les paramètres à ignorer")            
            await ctx.send(embed = embed)

    @purge.command(name = "kick")
    async def purge_kick(self, ctx, *members: discord.Member):
        """Kick members and send them DM"""
        kick_message = await self.config.guild(ctx.guild).kick_message()
        if kick_message is None:
            await ctx.send("Please set up kick message with !purge_setup")
            return

        members_id = []

        for member in members:
            try:
                await member.send(kick_message)
            except:
                pass
            members_id.append(member.id)
            await member.kick(reason = "Expulsé pour inactivité")
        
        today = date.today()
        for purge in self.purgeHistory:
            purge.member_list = [m for m in purge.member_list if m not in members_id]
            if purge.date == today:
                purge.member_list += members_id

        if not any(p.date == today for p in self.purgeHistory):
            purgeData = PurgeData(today, members_id)
            self.purgeHistory.append(purgeData)
        
        jsonhelper.save_json_objects(self.purge_history_filename, self.purgeHistory)

        await ctx.message.add_reaction('✅')

    @purge.command(name = "test")
    async def purge_test(self, ctx, *members:int):
        """Kick members and send them DM"""
        kick_message = await self.config.guild(ctx.guild).kick_message()
        if kick_message is None:
            await ctx.send("Please set up kick message with !purge_setup")
            return

        members_id = members

        today = date.today()
        for purge in self.purgeHistory:
            purge.member_list = [m for m in purge.member_list if m not in members_id]
            if purge.date == today:
                purge.member_list += members_id

        if not any(p.date == today for p in self.purgeHistory):
            purgeData = PurgeData(today, members_id)
            self.purgeHistory.append(purgeData)
        
        jsonhelper.save_json_objects(self.purge_history_filename, self.purgeHistory)

        await ctx.message.add_reaction('✅')

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def monitor(self, ctx):
        """Monitor activity"""
        pass

    @monitor.command(name = "server")
    async def monitor_server(self, ctx, duration = 0, when = 0):
        """
        Monitor server activity
        Utilisation: !monitor server <duree en jours> <il y a x jours>
        """
        async with ctx.channel.typing():
            progress_message = await ctx.send("Initialisation...")
            today = datetime.combine(date.today(), datetime.min.time())
            
            if when < 0:
                when = 0
            if duration <= 0 :
                duration = await self.config.guild(ctx.guild).days()

            after = when + duration -1
            before = when - 1

            before_date = today - timedelta(days = before)
            after_date = today - timedelta(days = after)
            
            before_date_str = before_date.strftime('%d-%m-%Y')
            after_date_str = after_date.strftime('%d-%m-%Y')
            

            ignored_channels = await self.config.guild(ctx.guild).ignored_channels()
            ignored_category = await self.config.guild(ctx.guild).ignored_category()

            active_members = set()
            channel_msg_count = {}
            category_msg_count = {}
            total_msg = 0
            lookup_channels = [c for c in ctx.guild.text_channels if c.id not in ignored_channels and c.category_id not in ignored_category]
            for channel in lookup_channels:
                await progress_message.edit(content=f"Recherche des membres actifs entre {after_date_str} et {before_date_str}. Salon: {channel}")

                channel_msg_count[channel] = 0
                if channel.category_id not in category_msg_count:
                    category_msg_count[channel.category_id] = 0
                
                async for message in channel.history(limit = None, after = after_date, before = before_date):
                    active_members.add(message.author)
                    channel_msg_count[channel] += 1
                
                category_msg_count[channel.category_id] += channel_msg_count[channel]
                total_msg += channel_msg_count[channel]
                

            await progress_message.delete()

            content = ""
            prev_category = None
            for channel, msg_count in channel_msg_count.items():
                if prev_category != channel.category_id:
                    if prev_category != None:
                        total_percent = (category_msg_count[prev_category]/total_msg) if total_msg > 0 else 0
                        content += f"Total : {category_msg_count[prev_category]} ({(total_percent):.2%})\n"
                        
                    prev_category = channel.category_id
                    content += f"\n{channel.category.name}\n"
                
                total_percent = (msg_count/total_msg) if total_msg > 0 else 0
                category_percent = (msg_count/category_msg_count[channel.category_id]) if category_msg_count[channel.category_id] > 0 else 0
                content += f" - {channel.mention} : {msg_count} ({total_percent:.2%}) ({category_percent:.2%})\n"
            
            total_percent = (category_msg_count[prev_category]/total_msg) if total_msg > 0 else 0
            content += f"Total : {category_msg_count[prev_category]} ({(total_percent):.2%})\n"


            embed = discord.Embed(title = f"Activité entre {after_date_str} et {before_date_str}", description = content, color = 0xffff00)
            await ctx.send(embed = embed)
                

            summary = f"{total_msg} messages\n"
            summary += f"{len(active_members)} membres actifs\n"
            summary += f"{len([m for m in ctx.guild.members if not m.bot])} membres"
            embed = discord.Embed(title = "Résumé", description = summary, color = 0xde2100)
            embed.set_footer(text = "Utilisez !purge_setup pour gérer les paramètres à ignorer")            
            await ctx.send(embed = embed)


    @monitor.command(name = "member")
    async def monitor_member(self, ctx, member: discord.Member, duration = 0):
        """
        Monitor Member activity
        Utilisation: !monitor member <membre> <duree en jours>
        """
        async with ctx.channel.typing():
            progress_message = await ctx.send("Initialisation...")
            today = datetime.combine(date.today(), datetime.min.time())
            
            if duration <= 0 :
                duration = await self.config.guild(ctx.guild).days()

            after = duration -1
            before = - 1

            before_date = today - timedelta(days = before)
            after_date = today - timedelta(days = after)
            
            before_date_str = before_date.strftime('%d-%m-%Y')
            after_date_str = after_date.strftime('%d-%m-%Y')
            

            ignored_channels = await self.config.guild(ctx.guild).ignored_channels()
            ignored_category = await self.config.guild(ctx.guild).ignored_category()

            channel_msg_count = {}
            category_msg_count = {}
            total_msg = 0
            lookup_channels = [c for c in ctx.guild.text_channels if c.id not in ignored_channels and c.category_id not in ignored_category]
            for channel in lookup_channels:
                await progress_message.edit(content=f"Recherche des membres actifs entre {after_date_str} et {before_date_str}. Salon: {channel}")

                channel_msg_count[channel] = 0
                if channel.category_id not in category_msg_count:
                    category_msg_count[channel.category_id] = 0
                
                async for message in channel.history(limit = None, after = after_date, before = before_date):
                    if message.author == member:
                        channel_msg_count[channel] += 1
                
                category_msg_count[channel.category_id] += channel_msg_count[channel]
                total_msg += channel_msg_count[channel]
                

            await progress_message.delete()

            content = ""
            prev_category = None
            for channel, msg_count in channel_msg_count.items():
                if prev_category != channel.category_id:
                    if prev_category != None:
                        total_percent = (category_msg_count[prev_category]/total_msg) if total_msg > 0 else 0
                        content += f"Total : {category_msg_count[prev_category]} ({(total_percent):.2%})\n"
                        
                    prev_category = channel.category_id
                    content += f"\n{channel.category.name}\n"
                
                total_percent = (msg_count/total_msg) if total_msg > 0 else 0
                category_percent = (msg_count/category_msg_count[channel.category_id]) if category_msg_count[channel.category_id] > 0 else 0
                content += f" - {channel.mention} : {msg_count} ({total_percent:.2%}) ({category_percent:.2%})\n"
            
            total_percent = (category_msg_count[prev_category]/total_msg) if total_msg > 0 else 0
            content += f"Total : {category_msg_count[prev_category]} ({(total_percent):.2%})\n"


            embed = discord.Embed(title = f"Activité entre {after_date_str} et {before_date_str}", description = content, color = 0xffff00)
            await ctx.send(embed = embed)
                

            summary = f"{total_msg} messages\n"
            embed = discord.Embed(title = f"Résumé de {member}", description = summary, color = 0xde2100)
            embed.set_footer(text = "Utilisez !purge_setup pour gérer les paramètres à ignorer")            
            await ctx.send(embed = embed)


################################################### CONFIGURATION

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def purge_setup(self, ctx):
        """Set up purge"""
        pass

    @purge_setup.command(name = "display")
    async def purge_setup_display(self, ctx): 
        """Display configuration"""
        days = await self.config.guild(ctx.guild).days()
        embed = discord.Embed(title = "Nombre de jours avant inactivité", description = days, color = 0xde2100)
        await ctx.send(embed = embed)

        channel_ids = await self.config.guild(ctx.guild).ignored_channels()
        channels = list(map(lambda c: str(c) if ctx.guild.get_channel(c) is None else ctx.guild.get_channel(c).mention, channel_ids))
        embed = discord.Embed(title = "Salons ignorés", description = "- " + "\n- ".join(channels), color = 0xde2100)
        embed.set_footer(text = "Total: {}".format(len(channels)))
        await ctx.send(embed = embed)

        category_ids = await self.config.guild(ctx.guild).ignored_category()
        categories = list(map(lambda c: str(c) if ctx.guild.get_channel(c) is None else ctx.guild.get_channel(c).mention, category_ids))
        embed = discord.Embed(title = "Categories ignorés", description = "- " + "\n- ".join(categories), color = 0xde2100)
        embed.set_footer(text = "Total: {}".format(len(categories)))
        await ctx.send(embed = embed)

        role_ids = await self.config.guild(ctx.guild).ignored_roles()
        roles = list(map(lambda r: ctx.guild.get_role(r).mention, role_ids))
        embed = discord.Embed(title = "Rôles ignorés", description = "- " + "\n- ".join(roles), color = 0xde2100)
        embed.set_footer(text = "Total: {}".format(len(roles)))
        await ctx.send(embed = embed)

        kick_message = await self.config.guild(ctx.guild).kick_message()
        embed = discord.Embed(title = "Message d'expulsion", description = kick_message if kick_message is not None else "", color = 0xde2100)
        await ctx.send(embed = embed)
    
    @purge_setup.command(name = "reset")
    async def purge_setup_reset(self, ctx): 
        """Reset configuration"""
        await self.config.guild(ctx.guild).ignored_channels.set([])
        await self.config.guild(ctx.guild).ignored_roles.set([])
        await self.config.guild(ctx.guild).days.set(30)

        await ctx.send("Done")

    @purge_setup.group(name = "ignored")
    async def purge_setup_ignored(self, ctx):
        """Set up parameters to be ignored"""
        pass

    @purge_setup_ignored.group(name = "channels")
    async def purge_setup_ignored_channels(self, ctx):
        """Set up channel to be ignored"""
        pass
    
    @purge_setup_ignored.group(name = "categories")
    async def purge_setup_ignored_categories(self, ctx):
        """Set up category to be ignored"""
        pass

    @purge_setup_ignored.group(name = "roles")
    async def purge_setup_ignored_roles(self, ctx):
        """Set up roles to be ignored"""
        pass

    @purge_setup_ignored_channels.command(name = "add")
    async def purge_setup_ignored_channels_add(self, ctx, *channels: discord.TextChannel):
        """Add channels to ignored ones"""
        ignored_channels = await self.config.guild(ctx.guild).ignored_channels()
        for channel in channels:
            if channel.id not in ignored_channels:
                ignored_channels.append(channel.id)
        await self.config.guild(ctx.guild).ignored_channels.set(ignored_channels)

        message = "Added {}".format(", ".join([c.mention for c in channels]))
        await ctx.send(message)
    
    @purge_setup_ignored_channels.command(name = "remove")
    async def purge_setup_ignored_channels_remove(self, ctx, *channels):
        """Remove channels to ignored ones"""
        ignored_channels = await self.config.guild(ctx.guild).ignored_channels()
        for channel in channels:
            if channel in ignored_channels:
                ignored_channels.remove(channel)
        await self.config.guild(ctx.guild).ignored_channels.set(ignored_channels)

        message = "Removed {}".format(", ".join([c for c in channels]))
        await ctx.send(message)

    @purge_setup_ignored_channels.command(name = "clear")
    async def purge_setup_ignored_channels_clear(self, ctx):
        """Clear channels to ignore"""
        await self.config.guild(ctx.guild).ignored_channels.set([])

        message = "Cleared channels to ignore"
        await ctx.send(message)

        
    @purge_setup_ignored_categories.command(name = "add")
    async def purge_setup_ignored_categories_add(self, ctx, *channels: discord.CategoryChannel):
        """Add channels to ignored ones"""
        ignored_category = await self.config.guild(ctx.guild).ignored_category()
        for channel in channels:
            if channel.id not in ignored_category:
                ignored_category.append(channel.id)
        await self.config.guild(ctx.guild).ignored_category.set(ignored_category)

        message = "Added {}".format(", ".join([c.mention for c in channels]))
        await ctx.send(message)
    
    @purge_setup_ignored_categories.command(name = "remove")
    async def purge_setup_ignored_categories_remove(self, ctx, *channels):
        """Remove channels to ignored ones"""
        ignored_category = await self.config.guild(ctx.guild).ignored_category()
        for channel in channels:
            if channel in ignored_category:
                ignored_category.remove(channel)
        await self.config.guild(ctx.guild).ignored_category.set(ignored_category)

        message = "Removed {}".format(", ".join([c for c in channels]))
        await ctx.send(message)


    @purge_setup_ignored_categories.command(name = "clear")
    async def purge_setup_ignored_categories_clear(self, ctx):
        """Clear categories to ignore"""
        await self.config.guild(ctx.guild).ignored_category.set([])

        message = "Cleared categories to ignore"
        await ctx.send(message)

    @purge_setup_ignored_roles.command(name = "add")
    async def purge_setup_ignored_roles_add(self, ctx, *roles: discord.Role):
        """Add roles to ignored ones"""
        ignored_roles = await self.config.guild(ctx.guild).ignored_roles()
        for role in roles:
            if role.id not in ignored_roles:
                ignored_roles.append(role.id)
        await self.config.guild(ctx.guild).ignored_roles.set(ignored_roles)

        message = "Added {}".format(", ".join([r.mention for r in roles]))
        await ctx.send(message)
    
    @purge_setup_ignored_roles.command(name = "remove")
    async def purge_setup_ignored_roles_remove(self, ctx, *roles: discord.Role):
        """Remove roles to ignored ones"""
        ignored_roles = await self.config.guild(ctx.guild).ignored_roles()
        for role in roles:
            if role.id in ignored_roles:
                ignored_roles.remove(role.id)
        await self.config.guild(ctx.guild).ignored_roles.set(ignored_roles)

        message = "Removed {}".format(", ".join([r.mention for r in roles]))
        await ctx.send(message)

    @purge_setup.command(name = "days")
    async def purge_setup_days(self, ctx, days: int):
        """Set up number of days before being considered as inactive"""
        await self.config.guild(ctx.guild).days.set(days)

        message = "Days before being inactive set up to {}".format(days)
        await ctx.send(message)

    @purge_setup.command(name = "kick_message")
    async def purge_setup_kick_message(self, ctx):
        """Setup the kick message"""

        def check(m):
            return m.author == ctx.author and m.channel == ctx.channel

        instructions = "You have 2 minutes to post in the next message the content of the kick message"
        instructions_msg = await ctx.send(instructions)
        try:
            message = await self.bot.wait_for('message', check=check, timeout=120)
            if message.content.strip() == "abort":
                raise Abort()
        except asyncio.TimeoutError:
            await ctx.send("{} Too late, please be quicker next time".format(ctx.author.mention))
        except Abort:
            await ctx.send("Edit aborted")
        else:
            await self.config.guild(ctx.guild).kick_message.set(message.content)
            confirmation = "Kick message configured on ```{}```".format(
                message.content)
            await ctx.send(confirmation)
            await message.add_reaction('✅')
        finally:
            await instructions_msg.delete()

################################################### HELPERS

    async def paginated_send(self, ctx, objects, header = ""):
        objects_message = ""
        for i, object in enumerate(objects, start = 1):
            objects_message_tmp = f" - {object.mention} ({object}, id: {object.id})\n"
            if (len(objects_message) + len(objects_message_tmp)) >= 4000:
                embed = discord.Embed(title = header, description = objects_message, color = 0x00aa00)
                embed.set_footer(text = f"{i}/{len(objects)}")
                await ctx.send(embed = embed)
                objects_message = ""
            objects_message += objects_message_tmp
        if len(objects_message) > 0:
            embed = discord.Embed(title = header, description = objects_message, color = 0x00aa00)
            embed.set_footer(text = f"{i}/{len(objects)}")
            await ctx.send(embed = embed)

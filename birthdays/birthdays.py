import asyncio
from datetime import date, datetime, time, timedelta
import calendar
import json
import os

import discord
from discord.ext import tasks
import logging
from redbot.core import Config, checks, commands, utils

import importlib.util
spec = importlib.util.spec_from_file_location("cogs.utility", "/cogs/utility/jsonhelper.py")
jsonhelper = importlib.util.module_from_spec(spec)
spec.loader.exec_module(jsonhelper)

log = logging.getLogger("red.birthdays")

class BirthdayData(jsonhelper.Serializable):
    def __init__(self, member_id: int, day:int, month:int):
        self.member_id = member_id
        self.day = day
        self.month = month
        
    def deserialize(object):
        return BirthdayData(int(object['member_id']), int(object['day']), int(object['month']))
    
    def serialize(self):
        return {
            'member_id':self.member_id,
            'day':self.day,
            'month':self.month
        }

class Birthdays(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20181201)
        default_guild = {
            "announce_channel": None,
            "announce_message": "",
            "join_message": ""
        }
        self.config.register_guild(**default_guild)
        self.birthday_file = "/cogs/birthdays/birthdays.json"
        
        self.anniv = jsonhelper.load_json_objects(self.birthday_file, BirthdayData)
        self.check_for_birthday.start()

    def cog_unload(self):
        log.info("Unload")
        self.check_for_birthday.cancel()
        
    @tasks.loop(hours=24.0)
    async def check_for_birthday(self):
        log.info("loop")
        now = datetime.now()

        tomorrow = date.today()
        tomorrow += timedelta(days=1)
        sleep = datetime.combine(tomorrow, time(8,0,0,0))

        sleep_timer = (sleep - now).total_seconds()
        if sleep_timer >= 600:
            log.info(f"Sleep {sleep_timer} seconds until {sleep}")
            await asyncio.sleep(sleep_timer + 60)
        
        log.info("Checking anniv")
        
        guilds = self.bot.guilds
        for guild in guilds:
            self.send_birthday_message(guild)
            self.send_joinday_message(guild)

    @commands.group()
    @commands.guild_only()
    async def anniv(self, ctx):
        pass

    @anniv.command(name="age")
    @checks.mod_or_permissions(administrator=True)
    async def anniv_wish(self, ctx: commands.Context):
        await self.send_birthday_message(ctx.guild)

    @anniv.command(name="join")
    @checks.mod_or_permissions(administrator=True)
    async def anniv_wishjoinday(self, ctx: commands.Context):
        await self.send_joinday_message(ctx.guild)

    @anniv.command(name="next")
    @checks.mod_or_permissions(administrator=True)
    async def anniv_next(self, ctx: commands.Context):
        today = date.today()
        announce_channel = self.bot.get_channel(await self.config.guild(ctx.guild).announce_channel())
        for birthday in self.anniv:
            if birthday.month >= today.month and birthday.day > today.day:
                member = ctx.guild.get_member(birthday.member_id)
                if member:
                    await ctx.send(f"Next birthday for {member.name} on {birthday.day:02}/{birthday.month:02}")
                    break

        next_member_birthday = None
        for member in announce_channel.members:
            if member.joined_at.day > today.day and member.joined_at.month >= today.month:
                if next_member_birthday == None or (member.joined_at.day < next_member_birthday.joined_at.day and member.joined_at.month <= next_member_birthday.joined_at.month):
                    next_member_birthday = member
                    
        await ctx.send(f"Next joinday for {next_member_birthday.name} on {next_member_birthday.joined_at.day:02}/{next_member_birthday.joined_at.month:02}")

    @anniv.command(name="add")
    @checks.mod_or_permissions(administrator=True)
    async def anniv_add(self, ctx: commands.Context, member:discord.Member, jour: int, mois:int):
        '''Add a birthday.'''
        await self._add_birthday(ctx, member, jour, mois)

    @anniv.command(name="del")
    @checks.mod_or_permissions(administrator=True)
    async def anniv_del(self, ctx: commands.Context, member:discord.Member):
        '''Remove a birthday.'''
        await self._remove_birthday(ctx, member)

    @anniv.command(name="ajout")
    async def anniv_ajout(self, ctx: commands.Context, jour: int, mois:int):
        '''Add my birthday.'''
        await self._add_birthday(ctx, ctx.message.author, jour, mois)

    @anniv.command(name="retire")
    async def anniv_retire(self, ctx: commands.Context):
        '''Remove my birthday.'''
        await self._remove_birthday(ctx, ctx.message.author)
    
    @anniv.command(name="list")
    @checks.mod_or_permissions(administrator=True)
    async def anniv_list(self, ctx: commands.Context):
        '''List anniv.'''
        birthday_list = f"Anniversaire de {ctx.guild.name}:\n```"
        for birthday in self.anniv:
            member = ctx.guild.get_member(birthday.member_id)
            if member:
                birthday_list += f"{birthday.day:02}/{birthday.month:02} : {member.name}\n"
        birthday_list += "```"
        await ctx.send(birthday_list)


##################################################
    async def send_birthday_message(self, guild):
        today = date.today()
        
        curmonth = today.month
        curday = today.day
        announce_channel = self.bot.get_channel(await self.config.guild(guild).announce_channel())
        for birthday in self.anniv:
            if birthday.month == curmonth and birthday.day == curday:
                
                keywords = {"MEMBER": birthday.member_id}
                message = (await self.config.guild(guild).announce_message()).format(**keywords)
                await announce_channel.send(message)

    async def send_joinday_message(self, guild):
        today = date.today()
        
        curmonth = today.month
        curday = today.day
        announce_channel = self.bot.get_channel(await self.config.guild(guild).announce_channel())
        for member in announce_channel.members:
            if member.joined_at.day == curday and member.joined_at.month == curmonth:
                keywords = {"MEMBER": member.mention, "ANNEE":today.year - member.joined_at.year}
                message = (await self.config.guild(guild).join_message()).format(**keywords)
                await announce_channel.send(message)

    async def _add_birthday(self, ctx, member, day, month):
        self.anniv = [b for b in self.anniv if b.member_id != member.id]
        birthday = BirthdayData(member.id, day, month)
        self.anniv.append(birthday)
        self.anniv = sorted(self.anniv, key=lambda b: b.month*30 + b.day)

        jsonhelper.save_json_objects(self.birthday_file, self.anniv)

        message = f"Added birthday for {member.name} at {day:02}/{month:02}"
        log.info(message)
        await ctx.send(message)

    async def _remove_birthday(self, ctx, member):
        self.anniv = [b for b in self.anniv if b.member_id != member.id]
        
        jsonhelper.save_json_objects(self.birthday_file, self.anniv)

        message = f"Anniversaire retiré pour {member.id}"
        log.info(message)
        await ctx.send(message)

##################################################
    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def anniv_setup(self, ctx):
        pass

    @anniv_setup.command(name="show", aliases=['s'])
    async def anniv_setup_show(self, ctx):
        """Show the configuration."""
        announce_channel = self.bot.get_channel(await self.config.guild(ctx.guild).announce_channel())
        await ctx.send(f"announce_channel: {announce_channel.mention}")
        message = (await self.config.guild(ctx.guild).join_message())
        await ctx.send(f"join message: {message}")
        message = (await self.config.guild(ctx.guild).announce_message())
        await ctx.send(f"birthday message: {message}")

    @anniv_setup.command(name="channel", aliases=['c'])
    async def anniv_setup_announcement_channel(self, ctx, channel: discord.TextChannel):
        """Setup the announcement channel."""
        await self.config.guild(ctx.guild).announce_channel.set(channel.id)

        message = "Announcement channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @anniv_setup.command(name="message", aliases=['m'])
    async def anniv_setup_announcement_message(self, ctx, *, message: str):
        """Setup the annoucement message to the announcement channel."""
        await self.config.guild(ctx.guild).announce_message.set(message)

        message = "Annoucement message configured on \"{}\"".format(message)
        log.info(message)
        await ctx.send(message)

    
    @anniv_setup.command(name="join", aliases=['j'])
    async def anniv_setup_join_message(self, ctx, *, message: str):
        """Setup the annoucement message to the join announcement channel."""
        await self.config.guild(ctx.guild).join_message.set(message)

        message = "Join message configured on \"{}\"".format(message)
        log.info(message)
        await ctx.send(message)
import asyncio
import discord
import logging

from typing import Union
from datetime import datetime, timedelta
from discord.ext import tasks
from redbot.core import Config, checks, commands

log = logging.getLogger("red.disboard_alert")

class DisboardAlert(commands.Cog):

    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20210312)
        default_guild = {
            "alert_delay": 120,
            "alert_dm_delta": 10,
            "alert_dm_user": None,
            "alert_channel": None
        }
        self.config.register_guild(**default_guild)
        self.skip_current = False


    def cog_unload(self):
        log.info("Unload")
        self.skip_current = True

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if not isinstance(message.channel, discord.TextChannel):
            return

        alert_channel = await self.config.guild(message.guild).alert_channel()

        if message.author.id == 302050872383242240 and message.channel.id == alert_channel and message.embeds[0].colour.value == 0x24b7b7:
            log.info("Disboard bump detected")

            alert_delay = await self.config.guild(message.guild).alert_delay()
            alert_dm_delta = await self.config.guild(message.guild).alert_dm_delta()
            alert_dm_user = self.bot.get_user(await self.config.guild(message.guild).alert_dm_user())
            start_time = datetime.utcnow()

            #log.info("Waiting before sending private message")
            #while datetime.utcnow() < start_time + timedelta(minutes = alert_delay - alert_dm_delta):
            #    await asyncio.sleep(30)

            #log.info("Sending private message")
            #embed = discord.Embed(description = "Bump Disboard dans {} minutes".format(alert_dm_delta))
            #embed.set_footer(text = "Utilisez `!disboard_alert abort` pour désactiver la prochaine alerte")
            #await alert_dm_user.send(embed = embed)

            log.info("Start waiting before sending alert")
            while datetime.utcnow() < start_time + timedelta(minutes = alert_delay) and not self.skip_current:
                # log.info("Waiting before sending alert")
                await asyncio.sleep(30)
            log.info("Finished waiting before sending alert")

            log.info(f"Skip ? {self.skip_current}")
            if not self.skip_current:
                await message.channel.send("C'est l'heure de faire `/bump`, `$bump` et `+bump`")
            
            self.skip_current = False
    
    @commands.group()
    @commands.dm_only()
    @checks.admin_or_permissions(administrator=True)
    async def disboard_alert(self, ctx):
        pass

    @disboard_alert.command(name = "abort")
    async def disboard_alert_abort(self, ctx):
        self.skip_current = True
        await ctx.message.add_reaction('✅')

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def disboard_alert_setup(self, ctx):
        """
        Set up disboard alerts
        """
        pass

    @disboard_alert_setup.group(name = "alert")
    async def das_alert(self, ctx):
        """
        Alert parameters
        """
        pass

    @das_alert.command(name = "delay")
    async def das_alert_delay(self, ctx, delay: int):
        """
        Send the alert after this delay
        """
        await self.config.guild(ctx.guild).alert_delay.set(delay)
        confirmation = "Delay configured on {}".format(delay)
        await ctx.send(confirmation)
    
    @das_alert.command(name = "dm_delta")
    async def das_alert_dm_delta(self, ctx, delta: int):
        """
        Send a dm <delta> minutes before the alert
        """
        await self.config.guild(ctx.guild).alert_dm_delta.set(delta)
        confirmation = "Delta configured on {}".format(delta)
        await ctx.send(confirmation)

    @das_alert.command(name = "dm_user")
    async def das_alert_dm_user(self, ctx, user: discord.User):
        """
        User whose dm will be send
        """
        await self.config.guild(ctx.guild).alert_dm_user.set(user.id)
        confirmation = "User configured on {}".format(user.mention)
        await ctx.send(confirmation)
    
    @das_alert.command(name = "channel")
    async def das_alert_channel(self, ctx, channel: discord.TextChannel):
        """
        Alert channel
        """
        await self.config.guild(ctx.guild).alert_channel.set(channel.id)
        confirmation = "Channel configured on {}".format(channel.mention)
        await ctx.send(confirmation)

    @disboard_alert_setup.command(name = "display")
    async def das_display(self, ctx):
        """
        Display configuration
        """
        alert_delay = await self.config.guild(ctx.guild).alert_delay()
        embed = discord.Embed(title = "Temps en minutes de l'envoi de l'alerte après bump", description = str(alert_delay), color = 0xde2100)
        await ctx.send(embed = embed)

        alert_dm_delta = await self.config.guild(ctx.guild).alert_dm_delta()
        embed = discord.Embed(title = "Temps en minutes de l'envoi d'un DM avant alerte", description = str(alert_dm_delta), color = 0xde2100)
        await ctx.send(embed = embed)

        alert_dm_user = (await self.bot.fetch_user(await self.config.guild(ctx.guild).alert_dm_user())).mention
        embed = discord.Embed(title = "DM envoyé à", description = alert_dm_user, color = 0xde2100)
        await ctx.send(embed = embed)

        alert_channel = (await self.bot.fetch_channel(await self.config.guild(ctx.guild).alert_channel())).mention
        embed = discord.Embed(title = "Salon spam", description = alert_channel, color = 0xde2100)
        await ctx.send(embed = embed)

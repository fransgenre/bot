import logging
import asyncio
from datetime import datetime, timedelta
from typing import List

import discord
from discord.ext import tasks
from redbot.core import Config, checks, commands

log = logging.getLogger("red.autodelete")


class Autodelete(commands.Cog):

    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=213452341245)
        self.purge_channels.start()

    def cog_unload(self):
        log.info("Unload")
        self.purge_channels.cancel()

    @commands.command(name="setautodel")
    @checks.admin_or_permissions(administrator=True)
    async def setautodel(self, ctx: commands.context, channel: discord.TextChannel):
        """
        Set the channel(s) where the auto-deletion has to occur.
        """
        await self.config.guild(ctx.guild).auto_purge_channel.set(channel.id)

        message = "The auto purge channel for guild " + str(ctx.guild.id) + " set to " + str(channel.name)
        log.info(message)
        await ctx.send(message)

    @commands.command(name="removeautodel")
    @checks.admin_or_permissions(administrator=True)
    async def removeautodel(self, ctx: commands.context):
        """
        Unsets the current autodel channel.
        """
        await self.config.guild(ctx.guild).auto_purge_channel.set(None)

    @tasks.loop(hours=1)
    async def purge_channels(self):
        log.info(f"loop")
        for current_guild in self.bot.guilds:
            if await self.config.guild(current_guild).auto_purge_channel():
                current_channel = self.bot.get_channel(await self.config.guild(current_guild).auto_purge_channel())
                messages_to_delete = await self.get_messages_for_deletion(current_channel)
                if len(messages_to_delete) > 0:
                    log.info(f"Purging {len(messages_to_delete)} message from {current_channel.name}")
                    for msg in messages_to_delete:
                        await msg.delete()
                        await asyncio.sleep(2)


    @commands.command(name="purge_channel")
    @checks.admin_or_permissions(administrator=True)
    async def purge_channel(self, ctx: commands.context, channel: discord.TextChannel):
        log.info("Purging channel {}".format(channel.name))
        await ctx.send("Purging channel {}".format(channel.name))
        msg_deleted = 1
        while msg_deleted != 0:
            messages_to_delete = await self.get_messages_for_deletion(channel)
            msg_deleted = len(messages_to_delete)
            if msg_deleted > 0:
                log.info("Purging {} message from {}, oldest created at {}".format(msg_deleted, channel.name, messages_to_delete[0].created_at))
                await ctx.send("Purging {} message from {}, oldest created at {}".format(msg_deleted, channel.name, messages_to_delete[0].created_at))
            msg_deleted = 0
            for msg in messages_to_delete:
                try:
                    await msg.delete()
                    await asyncio.sleep(2)
                    msg_deleted += 1
                except:
                    pass
            log.info("Purged {} messages from {}".format(msg_deleted, channel.name))
            await ctx.send("Purged {} messages from {}".format(msg_deleted, channel.name))
        log.info("Done purging channel {}".format(channel.name))
        await ctx.send("Done purging channel {}".format(channel.name))
            

    @purge_channels.before_loop
    async def before_purge(self):
        log.info("Waiting...")
        await self.bot.wait_until_ready()
        log.info("Ready!")

    async def get_messages_for_deletion(self, channel: discord.TextChannel) -> List[discord.Message]:
        """
        Auto deletes messages after X time unless there is an * attached with it.
        """
        time_limit = datetime.utcnow() - timedelta(weeks=3)
        messages = []

        # Scour the channel for messages
        async for message in channel.history(oldest_first=True,limit=1000):
            if (message.created_at < time_limit and '*' not in message.content) or not isinstance(message.author, discord.Member):
                messages.append(message)

        return messages

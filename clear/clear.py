import discord
import logging

from redbot.core import Config, checks, commands, utils
from redbot.core.utils.mod import mass_purge
from copy import copy
from datetime import datetime, timedelta
from typing import List, Union

import importlib.util
spec = importlib.util.spec_from_file_location("cogs.utility", "/cogs/utility/archive.py")
archive = importlib.util.module_from_spec(spec)
spec.loader.exec_module(archive)

log = logging.getLogger("red.clean")

class Clear(commands.Cog):

    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20181201)
        default_guild = {
            "archive_channel": None
        }
        self.config.register_guild(**default_guild)
    
    @commands.command()
    @checks.admin_or_permissions(administrator=True)
    async def clear(self, ctx: commands.Context, positive_int: int = 20):
        """
        Delete <positive_int> messages
        """
        if positive_int <= 0:
            await ctx.send_help(self.clear)
            return

        channel = ctx.channel
        author = ctx.author
        message = ctx.message

        log.info("Suppression de {} messages par {} dans {}".format(positive_int, author, channel))

        def is_not_pinned(m):
            return not m.pinned
            
        await channel.purge(before=message, limit=positive_int, check=is_not_pinned)
        await message.delete()

    
    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def archive(self, ctx: commands.Context):
        pass

    @archive.command(name="channel")
    async def archive_all(self, ctx: commands.Context):
        """
        Archive a channel
        """
        two_weeks_ago = datetime.utcnow() - timedelta(days=14, minutes=-5)
        if ctx.channel.created_at < two_weeks_ago:
            await ctx.send(content=f"Impossible d'archiver ce channel, il a ete cree il y a trop longtemps ({ctx.channel.created_at})", delete_after=10)
            return
        
        archive_message = await ctx.channel.history(limit=1000, oldest_first=True).flatten()
        
        archive_channel = self.bot.get_channel(await self.config.guild(ctx.guild).archive_channel())
        await archive._archive_helper(ctx, archive_channel, archive_message)

    @archive.command(name="after")
    async def archive_after(self, ctx: commands.Context, message_id:int):
        """
        Archive after a given message id
        """
        message = await ctx.channel.fetch_message(message_id)
        archive_message = await ctx.channel.history(before=ctx.message, after=message, oldest_first=True).flatten()
        
        archive_channel = self.bot.get_channel(await self.config.guild(ctx.guild).archive_channel())
        await archive._archive_helper(ctx, archive_channel, archive_message)

    @archive.command(name="between")
    async def archive_between(self, ctx: commands.Context, start_message_id:int, end_message_id:int):
        """
        Archive between two given message id
        """
        start_message = await ctx.channel.fetch_message(start_message_id)
        end_message = await ctx.channel.fetch_message(end_message_id)
        archive_message = await ctx.channel.history(before=end_message, after=start_message, oldest_first=True).flatten()
        
        archive_channel = self.bot.get_channel(await self.config.guild(ctx.guild).archive_channel())
        await archive._archive_helper(ctx, archive_channel, archive_message)

    @archive.command(name="messages")
    async def archive_messages(self, ctx: commands.Context, messages: int):
        """
        Archive a given number of messages
        """
        
        if messages <= 0:
            await ctx.send_help(self.clear)
            return
        archive_message = await ctx.channel.history(before=ctx.message, limit=messages).flatten()
        archive_message.reverse()
        
        archive_channel = self.bot.get_channel(await self.config.guild(ctx.guild).archive_channel())
        await archive._archive_helper(ctx, archive_channel, archive_message)

##################  HELPERS

    @staticmethod
    async def get_messages_for_deletion(channel,limit) -> List[discord.Message]:
        """
        Collect messages to delete
        """
        two_weeks_ago = datetime.utcnow() - timedelta(days=14, minutes=-5)
        collected = []
        
        async for message in channel.history(limit=limit):
            if message.created_at < two_weeks_ago:
                break
            if message.pinned:
                continue
            collected.append(message)
        
        return collected

        
##################  CONFIGURATION

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def clear_setup(self, ctx):
        pass

    @clear_setup.command(name="archive")
    async def clear_setup_archive_channel(self, ctx, channel: discord.TextChannel):
        """Setup the archive channel."""
        await self.config.guild(ctx.guild).archive_channel.set(channel.id)

        message = "Archive channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

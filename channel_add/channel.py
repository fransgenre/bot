import asyncio
import datetime as dt
import json

import discord
import logging
from redbot.core import Config, checks, commands, utils
from typing import Union

log = logging.getLogger("red.channelAdd")

class ChannelAdd(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20181201)
        default_guild = {
            "moderation_channel": None,
            "log_channel": None,
            "allow_emote": None,
            "ignore_emote": None,
            "deny_emote": None,
            "messages": {},
            "message_set": []
        }
        self.config.register_guild(**default_guild)

    def cog_unload(self):
        log.info("Unload")

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        guild = payload.member.guild
        message_set = await self.config.guild(guild).message_set()
        if payload.message_id in message_set and not payload.member.bot:
            await self._handle_staff_reaction(payload)
        else:
            try:
                channel_id = await self.config.guild(guild).get_raw('messages', payload.message_id)
                channel = self.bot.get_channel(channel_id)
                await self._handle_user_request(payload)
            except:
                pass


    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def channel_clear_awaiting(self, ctx):
        """"Stop waiting for reactions"""
        message_set = await self.config.guild(ctx.guild).message_set()
        messages = len(message_set)
        await self.config.guild(ctx.guild).message_set.set(set())
        message = "No longer awaiting reaction for {} messages".format(messages)
        log.info(message)
        await ctx.send(message)

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def allow(self, ctx, role_or_member:Union[discord.Member, discord.Role], *channels:discord.TextChannel):
        """"Allow channels to member or role"""
        for c in channels:
            await c.set_permissions(role_or_member,read_messages=True, send_messages=True)
        channels_str = [f"{c.mention} ({c.id})\n" for c in channels]
        message = f"{role_or_member.mention} a acces a:\n{channels_str}"
        log.info(message)
        await ctx.send(message)


    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def deny(self, ctx, role_or_member:Union[discord.Member, discord.Role], *channels:discord.TextChannel):
        """"Deny channels to member or role"""
        for c in channels:
            await c.set_permissions(role_or_member,read_messages=False, send_messages=False)
        channels_str = [f"{c.mention} ({c.id})\n" for c in channels]
        message = f"{role_or_member.mention} n'a plus acces a:\n{channels_str}"
        log.info(message)
        await ctx.send(message)

################################################## 

    async def _start_listening_to_message(self, guild, message_id):
        message_set = await self.config.guild(guild).message_set()
        message_set.append(message_id)
        await self.config.guild(guild).message_set.set(message_set)
        
    async def _stop_listening_to_message(self, guild, message_id):
        message_set = await self.config.guild(guild).message_set()
        message_set.remove(message_id)
        await self.config.guild(guild).message_set.set(message_set)

    async def _handle_user_request(self, payload: discord.RawReactionActionEvent):
        try:
            channel_id = await self.config.guild(payload.member.guild).get_raw('messages', payload.message_id)
            channel = self.bot.get_channel(channel_id)

            emote = await self._get_current_overload_emote(payload.member, channel)
            
            description = "{} souhaite accéder à {} ({})".format(payload.member.mention,channel.mention, emote)
            footer = "{}".format(channel_id)
            title = "{}".format(payload.member.id)
            name = "{}".format(payload.member)

            log.info(description)

            e = discord.Embed(colour=discord.Colour.teal(), title=title, description=description)
            e.set_footer(text=footer)

            if payload.member.avatar_url:
                e.set_author(name=name, icon_url=payload.member.avatar_url)
            else:
                e.set_author(name=name)
            
            mod_channel = self.bot.get_channel(await self.config.guild(payload.member.guild).moderation_channel())

            msg_sent = await mod_channel.send(embed=e)
            log.info("Sent {} by {}".format(msg_sent.id, msg_sent.author))
            await self._start_listening_to_message(payload.member.guild, msg_sent.id)
            
            allow_emote = await self.config.guild(payload.member.guild).allow_emote()
            ignore_emote = await self.config.guild(payload.member.guild).ignore_emote()
            deny_emote  = await self.config.guild(payload.member.guild).deny_emote()

            await msg_sent.add_reaction(allow_emote)
            await msg_sent.add_reaction(ignore_emote)
            await msg_sent.add_reaction(deny_emote)

            msg_channel = self.bot.get_channel(payload.channel_id)
            message = await msg_channel.fetch_message(payload.message_id)
            user = self.bot.get_user(payload.user_id)
            if not user:
                user = await self.bot.fetch_user(payload.user_id)
            # await message.remove_reaction(payload.emoji, user)

        except:
            pass
    
    
    async def _handle_staff_reaction(self, payload: discord.RawReactionActionEvent):
        guild = payload.member.guild
        message_set = await self.config.guild(guild).message_set()
        
        channel = self.bot.get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)

        allow_emote = await self.config.guild(guild).allow_emote()
        ignore_emote = await self.config.guild(guild).ignore_emote()

        member  = self.bot.get_user(int(message.embeds[0].title))
        channel = self.bot.get_channel(int(message.embeds[0].footer.text))

        prev_overload_emote = await self._get_current_overload_emote(member, channel)

        allow_member = allow_emote == payload.emoji.name
        log.info(f"Allow Emote: {allow_emote} == {payload.emoji.name}")
        log.info(f"Ignore Emote: {ignore_emote} == {payload.emoji.name}")
        log.info("{} asked to access {}, allowed? {}".format(member.name,channel.mention, allow_member))

        if ignore_emote != payload.emoji.name:
            await channel.set_permissions(member,read_messages=allow_member)

        await self._stop_listening_to_message(guild, payload.message_id)

        await message.delete()

        
        log_channel = self.bot.get_channel(await self.config.guild(guild).log_channel())

        await log_channel.send(f"Acces a {channel.mention} pour {member.mention} : {prev_overload_emote} ↦ {payload.emoji} ({payload.member})")

    async def _get_current_overload_emote(self, member: discord.Member, channel: discord.TextChannel):
        emote = "🔳"
        for m, overwrite in channel.overwrites.items():
            if m == member:
                allow, deny = overwrite.pair()
                if deny.read_messages:
                    emote = "❌"
                elif allow.read_messages:
                    emote = "✅"
                break
        return emote

##################################################
    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def channel_setup(self, ctx):
        pass

    @channel_setup.command(name="show")
    async def channel_setup_show(self, ctx):
        """Show the configuration."""
        config = await self.config.guild(ctx.guild).get_raw()
        message = ""
        for name, value in config.items():
            if "channel" in name and type(value) is int:
                message_temp = f"**{name}** : {ctx.guild.get_channel(value).mention}\n"
            elif "role" in name and type(value) is int:
                message_temp = f"**{name}** : {ctx.guild.get_role(value).mention}\n"
            elif type(value) is dict:
                value_str = str(json.dumps(value, indent=4, sort_keys=True))
                message_temp = f"**{name}** : \n```{value_str}```\n"
            elif type(value) is str:
                message_temp = f"**{name}** : \n```{value}```\n"
            else:
                message_temp = f"**{name}** : ({type(value)})\n```{value}```\n"

            if len(message) + len(message_temp) >= 4000:
                e = discord.Embed(colour=discord.Colour.red(), title="Configuration de Validation", description=message)
                await ctx.send(embed=e)
                message = ""
            if len(message_temp) >= 4096:
                message_temp = message_temp[:4080] + " ...```"
            message += message_temp
        e = discord.Embed(colour=discord.Colour.red(), title="Configuration de Validation", description=message)
        await ctx.send(embed=e)

    @channel_setup.command(name="log")
    async def channel_setup_log(self, ctx, channel: discord.TextChannel):
        """Setup the log channel."""
        await self.config.guild(ctx.guild).log_channel.set(channel.id)

        message = "Log channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @channel_setup.command(name="moderation", aliases=['mod'])
    async def channel_setup_moderation(self, ctx, channel: discord.TextChannel):
        """Setup the mod channel."""
        await self.config.guild(ctx.guild).moderation_channel.set(channel.id)

        message = "Moderation channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @channel_setup.command(name="allow")
    async def channel_setup_allow(self, ctx, emote):
        """Setup the allow emote."""
        await self.config.guild(ctx.guild).allow_emote.set(emote)

        message = "Allow emote configured on {}".format(emote)
        log.info(message)
        await ctx.send(message)

    @channel_setup.command(name="ignore")
    async def channel_setup_ignore(self, ctx, emote):
        """Setup the ignore emote."""
        await self.config.guild(ctx.guild).ignore_emote.set(emote)

        message = "Ignore emote configured on {}".format(emote)
        log.info(message)
        await ctx.send(message)

    @channel_setup.command(name="deny")
    async def channel_setup_deny(self, ctx, emote):
        """Setup the deny emote."""
        await self.config.guild(ctx.guild).deny_emote.set(emote)

        message = "Deny emote configured on {}".format(emote)
        log.info(message)
        await ctx.send(message)

    @channel_setup.command(name="add", aliases=['a'])
    async def channel_setup_add(self, ctx, message:int, channel: discord.TextChannel):
        """Add a channel."""
        await self.config.guild(ctx.guild).set_raw('messages', message, value=channel.id)

        message = "Add channel {} configured with message {}".format(channel.mention, message)
        log.info(message)
        await ctx.send(message)
    
    @channel_setup.command(name="del", aliases=['d'])
    async def channel_setup_del(self, ctx, message:int):
        """Remove a channel."""
        try:
            channel = self.bot.get_channel(await self.config.guild(ctx.guild).get_raw('messages', message))
            await self.config.guild(ctx.guild).clear_raw('messages', message)

            message = "Remove channel {} setup on message {}".format(channel.mention, message, type(message))
            log.info(message)
            await ctx.send(message)
        except KeyError:
            await ctx.send("No channel setup with message {}".format(message))
    
    @channel_setup.command(name="list", aliases=['l'])
    async def channel_setup_list(self, ctx):
        """List all channel."""
        data = await self.config.guild(ctx.guild).get_raw('messages')
        data_str = str(json.dumps(data, indent=4, sort_keys=True))
        for channel in ctx.guild.channels:
            data_str = data_str.replace(str(channel.id), channel.name)
        message = '```json\n{}```'.format(data_str)
        await ctx.send(message)

import asyncio
import datetime as dt
import json

import discord
import logging
from redbot.core import Config, checks, commands, utils
from redbot.core.utils.mod import mass_purge

log = logging.getLogger("red.vote")

class Vote(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20181201)
        default_guild = {
            "yes_emote": None,
            "no_emote": None,
            "blank_emote": None,
            "veto_emote": None,

            "moderation_channel": None,
            "history_channel": None,

            "user_vote_voting_channel": None,
            "user_vote_voting_message": None,
            "user_vote_validated_channel": None,
            "user_vote_validated_message": None,
            "user_vote_moderation_message": None,
            "user_vote_duration": 24,

            "validated_role": None,
            "mod_role": None,
            "staff_role": None,
            "member_role": None,
            "waitingValidation": [],
        }
        self.config.register_guild(**default_guild)

    def cog_unload(self):
        log.info("Unload")

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        waiting_validation_msg = await self.config.guild(payload.member.guild).waitingValidation()
        if payload.message_id not in waiting_validation_msg or payload.member.bot:
            return
        log.info("on_reaction_add known message")
        channel = self.bot.get_channel(await self.config.guild(payload.member.guild).moderation_channel())
        log.info("on_reaction_add known channel {}".format(channel))
        message = await channel.fetch_message(payload.message_id)
        log.info("on_reaction_add known message {}".format(message))
        member = message.mentions[0]
        log.info("Staff response to vote of {}".format(member))
        
        log.info("on_reaction_add end vote")
        await self._end_user_vote(member, payload.emoji.name)

        # On vire le message de reaction 
        log.info("on_reaction_add remove reaction")
        waiting_validation_msg.remove(payload.message_id)
        await self.config.guild(member.guild).waitingValidation.set(waiting_validation_msg)
        
        log.info("on_reaction_add purge channel")
        # Purge le channel de vote
        if len(waiting_validation_msg) == 0:
            user_vote_voting_channel = self.bot.get_channel(await self.config.guild(member.guild).user_vote_voting_channel())
            to_delete = await self._get_messages_for_deletion(user_vote_voting_channel)
            await mass_purge(to_delete, user_vote_voting_channel)

        log.info("on_reaction_add purge channel")
        
        await message.delete()

    @commands.command()
    async def end_user_vote(self, ctx, member: discord.Member, reaction: str):
        """"Promote a member"""
        await self._end_user_vote(member, reaction)
    
    @commands.command()
    async def vote(self, ctx, *, message: str):
        """"Start a simple vote"""
        await ctx.message.delete()
        await self._create_vote(ctx, message)
    
    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def user_vote(self, ctx, member: discord.Member):
        """"Start a vote for a user"""
        log.info("Start vote for {}".format(member))

        await ctx.message.delete()

        # Send voting message in the voting channel
        keywords = {"MEMBER": member.mention}
        message = (await self.config.guild(ctx.guild).user_vote_voting_message()).format(**keywords)

        user_vote_voting_channel = self.bot.get_channel(await self.config.guild(ctx.guild).user_vote_voting_channel())
        msg_sent = await user_vote_voting_channel.send(message)
        await self._add_all_emoji(ctx, msg_sent)


        # Wait for the vote to finish
        duration = int(await self.config.guild(ctx.guild).user_vote_duration())
        await asyncio.sleep(duration * 60 * 60)
        # await asyncio.sleep(15)


        # Send message to the moderation
        keywords = {"MEMBER": member.mention}
        message = (await self.config.guild(ctx.guild).user_vote_moderation_message()).format(**keywords)

        vote_msg = await user_vote_voting_channel.fetch_message(msg_sent.id)
        for reaction in vote_msg.reactions:
            message += f"\n{reaction.count} {reaction.emoji}"

        moderation_channel = self.bot.get_channel(await self.config.guild(ctx.guild).moderation_channel())
        msg_sent = await moderation_channel.send(message)


        # Wait for reaction
        waiting_validation_msg = await self.config.guild(member.guild).waitingValidation()
        if waiting_validation_msg == None:
            waiting_validation_msg = [msg_sent.id]
        else:
            waiting_validation_msg.append(msg_sent.id)
        await self.config.guild(member.guild).waitingValidation.set(waiting_validation_msg)
        await self._add_all_emoji(ctx, msg_sent)
    
    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def custom_role(self, ctx, member: discord.Member, hex_color:str, *, role_name:str):
        """"Create a custom role and give it to the user"""
        h=hex_color.lstrip('#')
        rgb=tuple(int(h[i:i+2], 16) for i in (0, 2, 4))
        color = discord.Color.from_rgb(rgb[0], rgb[1], rgb[2])
        
        validated_role = member.guild.get_role(await self.config.guild(member.guild).validated_role())
        mod_role       = member.guild.get_role(await self.config.guild(member.guild).mod_role())
        staff_role     = member.guild.get_role(await self.config.guild(member.guild).staff_role())
        roles = [validated_role, mod_role, staff_role]
        if member.top_role in roles:
            new_role = await ctx.guild.create_role(name=role_name, colour=color)
            await member.add_roles(new_role)
            
            position = {new_role: validated_role.position+1}
            await ctx.guild.edit_role_positions(positions=position)
        else:
            await member.top_role.edit(name=role_name,colour=color)

        await ctx.message.delete()



    @commands.command()
    async def clean_vote(self, ctx):
        """Remove waiting message from memory (in case of bug)"""
        await self.config.guild(ctx.guild).waitingValidation.set([])

        
    @commands.command()
    async def waited_vote(self, ctx):
        """Remove waiting message from memory (in case of bug)"""
        await ctx.send(await self.config.guild(ctx.guild).waitingValidation())


########################### HELPER

    async def _end_user_vote(self, member: discord.Member, reaction: str):
        """"Promote a member"""
        log.info("End vote of {}, result: {}".format(member, reaction))

        yes_emote = await self.config.guild(member.guild).yes_emote()

        if reaction == yes_emote:
            # on retire le role membre
            member_role = member.guild.get_role(await self.config.guild(member.guild).member_role())
            await member.remove_roles(member_role)

            # on ajoute le role de validation
            validated_role = member.guild.get_role(await self.config.guild(member.guild).validated_role())
            await member.add_roles(validated_role)
            
            # on poste le message de bienvenue
            keywords = {"MEMBER": member.mention}
            message = (await self.config.guild(member.guild).user_vote_validated_message()).format(**keywords)
            
            user_vote_validated_channel = self.bot.get_channel(await self.config.guild(member.guild).user_vote_validated_channel())
            await user_vote_validated_channel.send(message)


        # On historise le vote
        message = "Vote {} {} {} {}".format(member, member.mention, member.id, reaction)
        history_channel = self.bot.get_channel(await self.config.guild(member.guild).history_channel())
        await history_channel.send(message)

    async def _create_vote(self, ctx, message:str):
        e = discord.Embed(colour=discord.Colour.gold(), title="Vote", description=message)

        if ctx.author.avatar_url:
            e.set_author(name=ctx.author.name, icon_url=ctx.author.avatar_url)
        else:
            e.set_author(name=ctx.author.name)
        
        msg_sent = await ctx.send(embed=e)
        await self._add_all_emoji(ctx, msg_sent)

    async def _add_all_emoji(self, ctx, message:discord.Message):
        await self._add_yes_no_emoji(ctx, message)
        blank_emote = await self.config.guild(ctx.guild).blank_emote()
        # veto_emote  = await self.config.guild(ctx.guild).veto_emote()

        await message.add_reaction(blank_emote)
        # await message.add_reaction(veto_emote)

    async def _add_yes_no_emoji(self, ctx, message:discord.Message):
        yes_emote   = await self.config.guild(ctx.guild).yes_emote()
        no_emote    = await self.config.guild(ctx.guild).no_emote()
        
        await message.add_reaction(yes_emote)
        await message.add_reaction(no_emote)

    async def _get_messages_for_deletion(self, channel):
        """
        Collect messages to delete
        """
        collected = []
        
        async for message in channel.history():
            if message.pinned:
                continue
            collected.append(message)
        
        return collected

########################### CONFIGURATION

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def vote_setup(self, ctx):
        pass

    @vote_setup.group()
    async def user(self, ctx):
        pass

    @vote_setup.command(name="show")
    async def vote_setup_show(self, ctx):
        """Show the configuration"""
        yes_emote                    = await self.config.guild(ctx.guild).yes_emote()
        no_emote                     = await self.config.guild(ctx.guild).no_emote()
        blank_emote                  = await self.config.guild(ctx.guild).blank_emote()
        veto_emote                   = await self.config.guild(ctx.guild).veto_emote()
        moderation_channel           = await self.config.guild(ctx.guild).moderation_channel()
        history_channel              = await self.config.guild(ctx.guild).history_channel()
        user_vote_voting_channel     = await self.config.guild(ctx.guild).user_vote_voting_channel()
        user_vote_voting_message     = await self.config.guild(ctx.guild).user_vote_voting_message()
        user_vote_validated_channel  = await self.config.guild(ctx.guild).user_vote_validated_channel()
        user_vote_validated_message  = await self.config.guild(ctx.guild).user_vote_validated_message()
        user_vote_moderation_message = await self.config.guild(ctx.guild).user_vote_moderation_message()
        user_vote_duration           = await self.config.guild(ctx.guild).user_vote_duration()
        validated_role               = await self.config.guild(ctx.guild).validated_role()
        member_role                  = await self.config.guild(ctx.guild).member_role()
        mod_role                     = await self.config.guild(ctx.guild).mod_role()
        staff_role                   = await self.config.guild(ctx.guild).staff_role()

        message  = "Configuration:\n"
        message += "`\tyes_emote                    :` {}\n".format(yes_emote)
        message += "`\tno_emote                     :` {}\n".format(no_emote)
        message += "`\tblank_emote                  :` {}\n".format(blank_emote)
        message += "`\tveto_emote                   :` {}\n".format(veto_emote)
        message += "`\tmoderation_channel           :` <#{}>\n".format(moderation_channel)
        message += "`\tuser_vote_voting_channel     :` <#{}>\n".format(user_vote_voting_channel)
        message += "`\tuser_vote_voting_message     :` {}\n".format(user_vote_voting_message)
        message += "`\tuser_vote_validated_channel  :` <#{}>\n".format(user_vote_validated_channel)
        message += "`\tuser_vote_validated_message  :` {}\n".format(user_vote_validated_message)
        message += "`\tuser_vote_moderation_message :` {}\n".format(user_vote_moderation_message)
        message += "`\tuser_vote_duration           :` {}\n".format(user_vote_duration)
        message += "`\thistory_channel              :` <#{}>\n".format(history_channel)
        message += "`\tvalidated_role               :` <@&{}>\n".format(validated_role)
        message += "`\tmember_role                  :` <@&{}>\n".format(member_role)
        message += "`\tmod_role                     :` <@&{}>\n".format(mod_role)
        message += "`\tstaff_role                   :` <@&{}>\n".format(staff_role)
        # message += "```"

        await ctx.send(message)

    # USER CONFIGURATION
    @vote_setup.command(name="moderation", aliases=['mod'])
    async def vote_setup_moderation(self, ctx, channel: discord.TextChannel):
        """Setup the mod channel."""
        await self.config.guild(ctx.guild).moderation_channel.set(channel.id)

        message = "Moderation channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @user.command(name="history")
    async def vote_setup_history_channel(self, ctx, channel: discord.TextChannel):
        """Setup the history channel."""
        await self.config.guild(ctx.guild).history_channel.set(channel.id)

        message = "History channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @user.command(name="role")
    async def vote_setup_validated_role(self, ctx, role: discord.Role):
        """Setup the validated role."""
        await self.config.guild(ctx.guild).validated_role.set(role.id)

        message = "Validated role configured on {}".format(role.mention)
        log.info(message)
        await ctx.send(message)


    @vote_setup.command(name="member_role")
    async def vote_setup_member_role(self, ctx, role: discord.Role):
        """Setup the member role."""
        await self.config.guild(ctx.guild).member_role.set(role.id)

        message = "Member role configured on {}".format(role.mention)
        log.info(message)
        await ctx.send(message)

    @vote_setup.command(name="mod_role")
    async def vote_setup_mod_role(self, ctx, role: discord.Role):
        """Setup the mod role."""
        await self.config.guild(ctx.guild).mod_role.set(role.id)

        message = "Mod role configured on {}".format(role.mention)
        log.info(message)
        await ctx.send(message)

    @user.command(name="staff_role")
    async def vote_setup_staff_role(self, ctx, role: discord.Role):
        """Setup the staff role."""
        await self.config.guild(ctx.guild).staff_role.set(role.id)

        message = "Staff role configured on {}".format(role.mention)
        log.info(message)
        await ctx.send(message)

    @user.command(name="message", aliases=['um'])
    async def vote_setup_user_message(self, ctx, *, message: str):
        """Setup the user vote message."""
        await self.config.guild(ctx.guild).user_vote_voting_message.set(message)

        message = "User vote message configured on \"{}\"".format(message)
        log.info(message)
        await ctx.send(message)

    @user.command(name="validated_message", aliases=['uvm'])
    async def vote_setup_user_validated_message(self, ctx, *, message: str):
        """Setup the user vote validation message."""
        await self.config.guild(ctx.guild).user_vote_validated_message.set(message)

        message = "User vote validation message configured on \"{}\"".format(message)
        log.info(message)
        await ctx.send(message)

    @user.command(name="channel", aliases=['uc'])
    async def vote_setup_user_channel(self, ctx, channel: discord.TextChannel):
        """Setup the user vote channel."""
        await self.config.guild(ctx.guild).user_vote_voting_channel.set(channel.id)

        message = "User vote channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @user.command(name="validated_channel", aliases=['uvc'])
    async def vote_setup_user_validated_channel(self, ctx, channel: discord.TextChannel):
        """Setup the user vote validated channel."""
        await self.config.guild(ctx.guild).user_vote_validated_channel.set(channel.id)

        message = "User vote channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @user.command(name="moderation_message", aliases=['uvmm'])
    async def vote_setup_user_moderation_message(self, ctx, *, message: str):
        """Setup the user vote moderation message."""
        await self.config.guild(ctx.guild).user_vote_moderation_message.set(message)

        message = "User vote moderation message configured on \"{}\"".format(message)
        log.info(message)
        await ctx.send(message)

    @user.command(name="duration", aliases=['ud'])
    async def vote_setup_user_duration(self, ctx, duration: int):
        """Setup the user vote message."""
        await self.config.guild(ctx.guild).user_vote_duration.set(duration)

        message = "User vote duration is now {} hours".format(duration)
        log.info(message)
        await ctx.send(message)

    # EMOTE CONFIGURATION
    @vote_setup.command(name="yes", aliases=['y'])
    async def vote_setup_no(self, ctx, emote: str):
        """Setup the yes emote."""
        await self.config.guild(ctx.guild).yes_emote.set(emote)

        message = "Yes emote configured on {}".format(emote)
        log.info(message)
        await ctx.send(message)

    @vote_setup.command(name="no", aliases=['n'])
    async def vote_setup_deny(self, ctx, emote: str):
        """Setup the no emote."""
        await self.config.guild(ctx.guild).no_emote.set(emote)

        message = "No emote configured on {}".format(emote)
        log.info(message)
        await ctx.send(message)

    @vote_setup.command(name="blank", aliases=['b'])
    async def vote_setup_blank(self, ctx, emote: str):
        """Setup the blank emote."""
        await self.config.guild(ctx.guild).blank_emote.set(emote)

        message = "Allow blank configured on {}".format(emote)
        log.info(message)
        await ctx.send(message)
    
    @vote_setup.command(name="veto", aliases=['v'])
    async def vote_setup_veto(self, ctx, emote: str):
        """Setup the veto emote."""
        await self.config.guild(ctx.guild).veto_emote.set(emote)

        message = "Veto emote configured on {}".format(emote)
        log.info(message)
        await ctx.send(message)
    
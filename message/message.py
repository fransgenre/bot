import asyncio
import datetime as dt
import json

import discord
import logging
from redbot.core import Config, checks, commands, utils
from redbot.core.utils.chat_formatting import pagify

log = logging.getLogger("red.message")

class Message(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20181201)
        default_guild = {
            "moderation_channel": None
        }
        self.config.register_guild(**default_guild)

    def cog_unload(self):
        log.info("Unload")

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def sendDM(self, ctx: commands.Context, member: discord.Member, *, message: str):
        """Sends a DM to a user."""
        if member is None or member.bot:
            await ctx.send(
                "Utilisateur invalide ou bot. "
                "On ne peux envoyer des messages qu'aux utilisateur avec qui je partage un serveur."
            )
            return

        prefixes = await ctx.bot.get_valid_prefixes()
        description = "{}".format(ctx.bot.user)
        content = "Pour répondre à ce message utiliser la commande {}reponse".format(prefixes[0])
        
        if await ctx.embed_requested():
            e = discord.Embed(colour=discord.Colour.red(), description=message)

            e.set_footer(text=content)
            if ctx.bot.user.avatar_url:
                e.set_author(name=description, icon_url=ctx.bot.user.avatar_url)
            else:
                e.set_author(name=description)

            try:
                await member.send(embed=e)
            except discord.HTTPException:
                await ctx.send("Erreur d'envoi du message à {}".format(member))
            else:
                await ctx.send("Message envoyé à {}".format(member))
        else:
            response = "{}\nMessage:\n\n{}".format(description, message)
            try:
                await member.send("{}\n{}".format(response, content))
                await member.send("{}".format(response))
            except discord.HTTPException:
                await ctx.send("Erreur d'envoi du message à {}".format(member))
            else:
                await ctx.send("Message envoyé à {}".format(member))

        for attachment in ctx.message.attachments:
            file = await attachment.to_file()
            await member.send(file=file)

    @commands.command()
    async def reponse(self, ctx: commands.Context, *, message: str):
        """Sends a DM response to the moderation."""
        
        description = "{}".format(ctx.author)
        response = "{}\n\n{}".format(description, message)
        
        if await ctx.embed_requested():
            e = discord.Embed(colour=discord.Colour.red(), description=message)

            if ctx.author.avatar_url:
                e.set_author(name=description, icon_url=ctx.author.avatar_url)
            else:
                e.set_author(name=description)
            
            guilds = self.bot.guilds
            for guild in guilds:
                channel = self.bot.get_channel(await self.config.guild(guild).moderation_channel())
                await channel.send(embed=e)
                for attachment in ctx.message.attachments:
                    file = await attachment.to_file()
                    await channel.send(file=file)
        else:
            guilds = self.bot.guilds
            for guild in guilds:
                channel = await self.bot.get_channel(await self.config.guild(guild).moderation_channel())
                await channel.send(response)
                for attachment in ctx.message.attachments:
                    file = await attachment.to_file()
                    await channel.send(file=file)

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def message(self, ctx: commands.Context, channel: discord.TextChannel, *, message: str):
        """Sends an message in a text channel."""
        msg_sent = await channel.send(message)
        await ctx.send("Successfuly sent msg {}".format(msg_sent.id))

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def copy(self, ctx: commands.Context, channel: discord.TextChannel):
        """Sends a copy of the mentionned message in a text channel."""
        content = await ctx.channel.fetch_message(ctx.message.reference.message_id)
        for page in pagify(content.content):
            msg_sent = await channel.send(f"{page}")
            await ctx.send(f"Successfuly copied msg to {channel.mention}, message ID: {msg_sent.id}")

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def copyAll(self, ctx: commands.Context, channel: discord.TextChannel):
        """Sends a copy of the mentionned message in a text channel."""
        async for message in channel.history(limit=200, oldest_first=True):
            for page in pagify(message.content):
                await channel.send(f"{page}")
    

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def messageEmbed(self, ctx: commands.Context, channel: discord.TextChannel, *, message: str):
        """Sends an embed message in a text channel."""
        e = discord.Embed(colour=discord.Colour.red(), description=message)
        
        msg_sent = await channel.send(embed=e)
        await ctx.send("Successfuly sent msg {}".format(msg_sent.id))

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def react(self, ctx: commands.Context, channel: discord.TextChannel, message_id:int, emote: str):
        """React to a message."""
        message = await channel.fetch_message(message_id)
        await message.add_reaction(emote)
    
    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def edit(self, ctx: commands.Context, channel: discord.TextChannel, message_id:int, *, edit: str):
        """Edit a message."""
        try:
            message = await channel.fetch_message(message_id)
            await message.edit(content=edit)
            await ctx.send("Editing successful")
        except HTTPException as e:
            await ctx.send("Editing forbidden\nResponse: {}\nMessage:{}".format(e.response,e.text))
        except:
            await ctx.send("Editing error")

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def dm_setup(self, ctx):
        pass

    @dm_setup.command(name="moderation", aliases=['mod'])
    async def dm_setup_moderation(self, ctx, channel: discord.TextChannel):
        """Setup the mod channel."""
        await self.config.guild(ctx.guild).moderation_channel.set(channel.id)

        message = "DM Moderation channel configured on {}".format(channel.mention)
        await ctx.send(message)


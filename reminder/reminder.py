from datetime import date, datetime, timedelta

import discord
from discord.ext import tasks
import logging
from redbot.core import Config, checks, commands, utils

import importlib.util
spec = importlib.util.spec_from_file_location("cogs.utility", "/cogs/utility/jsonhelper.py")
jsonhelper = importlib.util.module_from_spec(spec)
spec.loader.exec_module(jsonhelper)

log = logging.getLogger("red.reminder")

class ReminderData(jsonhelper.Serializable):
    def __init__(self, id: int, date:datetime, frequency_quantity:int, frequency_unit:str, text:str):
        self.id = id
        self.date = date
        self.text = text
        self.frequency_quantity = frequency_quantity
        self.frequency_unit = frequency_unit
        
    def deserialize(object):
        if 'frequency_quantity' in object:
            return ReminderData(object['id'], datetime.fromisoformat(object['date']), object['frequency_quantity'], object['frequency_unit'], object['text'])
        else:
            return ReminderData(object['id'], datetime.fromisoformat(object['date']), 0, "", object['text'])
    
    def serialize(self):
        if self.frequency_quantity > 0:
            return {
                'id':self.id,
                'date':str(self.date),
                'frequency_quantity':self.frequency_quantity,
                'frequency_unit':self.frequency_unit,
                'text':self.text
            }
        else:
            return {
                'id':self.id,
                'date':str(self.date),
                'text':self.text
            }

class Reminder(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.reminders_filename = "/cogs/reminder/reminder.json"
        self.reminders = jsonhelper.load_json_objects(self.reminders_filename, ReminderData)
        self.units = {"seconde" : 1, "minute" : 60, "heure" : 3600, "jour" : 86400, "semaine": 604800, "moi": 2592000, "an": 31104000}

        self.config = Config.get_conf(self, identifier=20181201)
        default_guild = {
            "reminder_channel": None,
            "admin_role": None
        }
        self.config.register_guild(**default_guild)
        
        self.check_reminders.start()

    def cog_unload(self):
        log.info("Unload")
        self.check_reminders.cancel()

    @tasks.loop(minutes=15.0)
    async def check_reminders(self):
        to_remove = []
        to_add = []
        now = datetime.utcnow()
        guilds = self.bot.guilds
        
        for guild in guilds:
            reminder_channel = self.bot.get_channel(await self.config.guild(guild).reminder_channel())
            
            for reminder in self.reminders:
                if reminder.date <= now:
                    try:
                        log.info("Sending reminders")
                        e = discord.Embed(colour=discord.Colour.red(), title="Rappel", description=reminder.text)
        
                        await reminder_channel.send(embed=e)
                    except:
                        pass
                    else:
                        to_remove.append(reminder)
                        if reminder.frequency_quantity > 0:
                            seconds = self.units[reminder.frequency_unit] * reminder.frequency_quantity
                            reminder_date = now + timedelta(seconds = seconds)
                            reminder_copy = ReminderData(reminder.id, reminder_date, reminder.frequency_quantity, reminder.frequency_unit, reminder.text)
                            to_add.append(reminder_copy)
                            
        
        for reminder in to_remove:
            self.reminders.remove(reminder)
            
        for reminder in to_add:
            self.reminders.append(reminder)

        if to_remove or to_add:
            log.info("Update reminders list")
            jsonhelper.save_json_objects(self.reminders_filename, self.reminders)
        
    
    @check_reminders.before_loop
    async def before_check_reminders(self):
        log.info("Waiting for bot to be ready")
        await self.bot.wait_until_ready()
        log.info("Bot is ready, starting reminder checking")

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def rappel(self, ctx):
        pass

    @rappel.command(name="dans")
    @checks.mod_or_permissions(administrator=True)
    async def rappel_dans(self, ctx,  quantity : int, time_unit : str, *, text : str):
        """
        Sends you <text> when the time is up
        Accepts: minute, heure, jour, semaine, mois, ans
        Example:
        !rappel dans 3 jours Have sushi with Asu and JennJenn
        """
        unit = time_unit.lower()
        
        if unit.endswith("s"):
            unit = unit[:-1]
        
        if not unit in self.units:
            await ctx.send("Unite de temps invalide. Choix possible: minute, heure, jour, semaine, mois, ans")
            return
        
        if quantity < 1:
            await ctx.send("La duree doit etre strictement superieure a 0..")
            return
                
        seconds = self.units[unit] * quantity
        today = datetime.utcnow()
        reminder_date = today + timedelta(seconds = seconds)
        reminder = ReminderData(ctx.message.id, reminder_date, 0 , "", text)
        
        self.reminders.append(reminder)

        author = ctx.message.author
        log.info("{} ({}) set a reminder.".format(author.name, author.id))
        await ctx.send(f"Rappel ajoute dans {quantity} {time_unit}, le {reminder_date}.")

        jsonhelper.save_json_objects(self.reminders_filename, self.reminders)

    @rappel.command(name="chaque")
    @checks.mod_or_permissions(administrator=True)
    async def rappel_chaque(self, ctx,  quantity : int, time_unit : str, *, text : str):
        """
        Sends you <text> when the time is up and repeats
        Accepts: minute, heure, jour, semaine, mois, ans
        Example:
        !rappel chaque 3 jours Have sushi with Asu and JennJenn
        """
        unit = time_unit.lower()
        
        if unit.endswith("s"):
            unit = unit[:-1]
        
        if not unit in self.units:
            await ctx.send("Unite de temps invalide. Choix possible: minute, heure, jour, semaine, mois, ans")
            return
        
        if quantity < 1:
            await ctx.send("La duree doit etre strictement superieure a 0.")
            return
                
        seconds = self.units[unit] * quantity
        today = datetime.utcnow()
        reminder_date = today + timedelta(seconds = seconds)
        reminder = ReminderData(ctx.message.id, reminder_date, quantity, unit, text)
        
        self.reminders.append(reminder)

        author = ctx.message.author
        log.info("{} ({}) set a reminder.".format(author.name, author.id))
        await ctx.send(f"Rappel ajoute chaque {quantity} {time_unit}, le prochain le {reminder_date}.")

        jsonhelper.save_json_objects(self.reminders_filename, self.reminders)

    @rappel.command(name="lechaque")
    @checks.mod_or_permissions(administrator=True)
    async def rappel_lechaque(self, ctx, date : str, quantity : int, time_unit : str, *, text : str):
        """
        Sends you <text> when the date is reached and repeats
        Example:
        !rappel lechaque 2021-01-01T01:01:00 3 jours Have sushi with Asu and JennJenn
        !rappel lechaque 2021-01-01 3 jours Have sushi with Asu and JennJenn
        """
        unit = time_unit.lower()
        
        if unit.endswith("s"):
            unit = unit[:-1]
        
        if not unit in self.units:
            await ctx.send("Unite de temps invalide. Choix possible: minute, heure, jour, semaine, mois, ans")
            return
        
        if quantity < 1:
            await ctx.send("La duree doit etre strictement superieure a 0.")
            return
                
        reminder_date = datetime.fromisoformat(date)
        reminder = ReminderData(ctx.message.id, reminder_date, quantity, unit, text)
        
        self.reminders.append(reminder)

        author = ctx.message.author
        log.info("{} ({}) set a reminder.".format(author.name, author.id))
        await ctx.send(f"Rappel ajoute le {reminder_date}, chaque {quantity} {time_unit}.")

        jsonhelper.save_json_objects(self.reminders_filename, self.reminders)

    @rappel.command(name="le")
    @checks.mod_or_permissions(administrator=True)
    async def rappel_le(self, ctx, date : str, *, text : str):
        """
        Sends you <text> when the date is reached
        Example:
        !rappel le 2021-01-01T01:01:00 Have sushi with Asu and JennJenn
        !rappel le 2021-01-01 Have sushi with Asu and JennJenn
        """
        
        reminder_date = datetime.fromisoformat(date)
        reminder = ReminderData(ctx.message.id, reminder_date, 0, "", text)
        
        self.reminders.append(reminder)

        author = ctx.message.author
        log.info("{} ({}) set a reminder.".format(author.name, author.id))
        await ctx.send(f"Rappel ajoute le {reminder_date}.")

        jsonhelper.save_json_objects(self.reminders_filename, self.reminders)


    @rappel.command(name="list")
    @checks.mod_or_permissions(administrator=True)
    async def rappel_list(self, ctx):
        """List all your upcoming notifications"""
        for reminder in self.reminders:
            if reminder.frequency_quantity > 0:
                await ctx.send(f"*{reminder.id}* le **{reminder.date}**, repete chaque {reminder.frequency_quantity} {reminder.frequency_unit}:\n```{reminder.text}```")
            else:
                await ctx.send(f"*{reminder.id}* le **{reminder.date}**:\n```{reminder.text}```")
        
        await ctx.message.add_reaction('✅')


    @rappel.command(name="del")
    @checks.mod_or_permissions(administrator=True)
    async def rappel_del(self, ctx, id:int):
        """Removes all your upcoming notifications"""
        to_remove = [r for r in self.reminders if r.id == id]
        
        if not to_remove == []:
            for reminder in to_remove:
                self.reminders.remove(reminder)
            
            jsonhelper.save_json_objects(self.reminders_filename, self.reminders)
            
            await ctx.send("Reminders removed.")
        else:
            await ctx.send(f"No reminders with id {id}.")

    

##################################################
    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def rappel_setup(self, ctx):
        pass

    @rappel_setup.command(name="channel")
    async def rappel_setup_reminder_channel(self, ctx, channel: discord.TextChannel):
        """Setup the reminder channel."""
        await self.config.guild(ctx.guild).reminder_channel.set(channel.id)

        message = "Reminder channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @rappel_setup.command(name="role")
    async def rappel_setup_admin_role(self, ctx, role: discord.Role):
        """Setup the admin role."""
        await self.config.guild(ctx.guild).admin_role.set(role.id)

        message = "Admin role configured on {}".format(role.name)
        log.info(message)
        await ctx.send(message)

import asyncio
import datetime as dt
import json

import discord
import logging
from redbot.core import Config, checks, commands, utils
from redbot.core.utils.mod import mass_purge
from datetime import datetime, timedelta

import importlib.util
spec = importlib.util.spec_from_file_location("cogs.utility", "/cogs/utility/archive.py")
archive = importlib.util.module_from_spec(spec)
spec.loader.exec_module(archive)

class Convocation(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20181201)
        default_guild = {
            "moderation_channel": None,
            "convocation_channel": None,
            "convocation_message": None,
            "convocation_welcome": None,
            "convocation_role": None,
            "staff_role": None,
        }
        self.config.register_guild(**default_guild)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        guild = payload.member.guild
        convocation_message = await self.config.guild(guild).convocation_message()
        if payload.message_id != convocation_message or payload.member.bot:
            return

        await self._create_convocation(guild, payload.member)
        await self._remove_reaction(payload)


########################### HELPER

    async def _remove_reaction(self, payload: discord.RawReactionActionEvent):
        msg_channel = self.bot.get_channel(payload.channel_id)
        message = await msg_channel.fetch_message(payload.message_id)
        user = self.bot.get_user(payload.user_id)
        if not user:
            user = await self.bot.fetch_user(payload.user_id)
        await message.remove_reaction(payload.emoji, user)

    async def _create_convocation(self, guild:discord.Guild, member:discord.Member):
        # On donne le role
        convocation_role = guild.get_role(await self.config.guild(guild).convocation_role())
        staff_role = guild.get_role(await self.config.guild(guild).staff_role())
        moderation_channel = guild.get_channel(await self.config.guild(guild).moderation_channel())

        if convocation_role in member.roles:
            return False

        overwrites = {
            guild.default_role: discord.PermissionOverwrite(view_channel=False, read_messages=False, send_messages=False),
            staff_role: discord.PermissionOverwrite(view_channel=True, read_messages=True, send_messages=True),
            member: discord.PermissionOverwrite(view_channel=True, read_messages=True, send_messages=True)
        }
        
        channel_name = "bureau-{}".format(member.name)
        convocation_channel = await moderation_channel.category.create_text_channel(channel_name, 
                                                              overwrites=overwrites,
                                                              position=0,
                                                              slowmode_delay=5)
        
        await member.add_roles(convocation_role)
        
        # On poste le message de bienvenue
        keywords = {"MEMBER": member.mention}
        message = (await self.config.guild(guild).convocation_welcome()).format(**keywords)
        
        await convocation_channel.send(message)

    @commands.command()
    @checks.admin_or_permissions(administrator=True)
    async def end_convocation(self, ctx: commands.Context):
        """
        Close convocation channel
        """
        two_weeks_ago = datetime.utcnow() - timedelta(days=14, minutes=-5)
        if ctx.channel.created_at < two_weeks_ago:
            await ctx.send(content=f"Protection du channel, il ne sera pas suprimer, il a ete cree il y a trop longtemps ({ctx.channel.created_at})", delete_after=10)
            return
        
        await self.end_convocation_force(ctx)

    @commands.command()
    @checks.admin_or_permissions(administrator=True)
    async def end_convocation_force(self, ctx: commands.Context):
        """
        Close convocation channel
        """
        
        channel = ctx.channel
        convocation_role = ctx.guild.get_role(await self.config.guild(ctx.guild).convocation_role())
        for member in channel.members:
            if convocation_role in member.roles:
                await member.remove_roles(convocation_role)
                
        archive_message = await channel.history(limit=1000, oldest_first=True).flatten()

        # Hard coded Historique channel
        archive_channel = self.bot.get_channel(525005075962396743)
        await archive._archive_helper(ctx, archive_channel, archive_message)

        await channel.delete()

    @commands.command()
    @checks.admin_or_permissions(administrator=True)
    async def start_convocation(self, ctx: commands.Context, member: discord.Member):
        """
        Create convocation channel
        """
        await self._create_convocation(ctx.guild, member)

########################### CONFIGURATION

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def convocation_setup(self, ctx):
        pass

    @convocation_setup.command(name="show")
    async def convocation_setup_show(self, ctx):
        """Show the configuration"""
        convocation_channel = await self.config.guild(ctx.guild).convocation_channel()
        moderation_channel  = await self.config.guild(ctx.guild).moderation_channel()
        convocation_message = await self.config.guild(ctx.guild).convocation_message()
        convocation_welcome = await self.config.guild(ctx.guild).convocation_welcome()
        convocation_role    = await self.config.guild(ctx.guild).convocation_role()
        staff_role          = await self.config.guild(ctx.guild).staff_role()

        message  = "```Configuration:\n"
        message += "\tconvocation_channel : {}\n".format(convocation_channel)
        message += "\tmoderation_channel  : {}\n".format(moderation_channel)
        message += "\tconvocation_message  : {}\n".format(convocation_message)
        message += "\tconvocation_welcome : {}\n".format(convocation_welcome)
        message += "\tconvocation_role    : {}\n".format(convocation_role)
        message += "\tstaff_role          : {}\n".format(staff_role)
        message += "```"

        await ctx.send(message)

    # USER CONFIGURATION
    @convocation_setup.command(name="moderation")
    async def convocation_setup_moderation_channel(self, ctx, channel: discord.TextChannel):
        """Setup the moderation channel."""
        await self.config.guild(ctx.guild).moderation_channel.set(channel.id)

        message = "Moderation channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @convocation_setup.command(name="channel")
    async def convocation_setup_channel(self, ctx, channel: discord.TextChannel):
        """Setup the convocation channel."""
        await self.config.guild(ctx.guild).convocation_channel.set(channel.id)

        message = "Convocation channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @convocation_setup.command(name="message")
    async def convocation_setup_message(self, ctx, message: int):
        """Setup the message with the reaction."""
        await self.config.guild(ctx.guild).convocation_message.set(message)

        message = "Convocation message configured on {}".format(message)
        log.info(message)
        await ctx.send(message)

    @convocation_setup.command(name="welcome")
    async def convocation_setup_welcome(self, ctx, *, welcome: str):
        """Setup the welcome message to the new channel."""
        await self.config.guild(ctx.guild).convocation_welcome.set(welcome)

        message = "Convocation welcome message configured on \"{}\"".format(welcome)
        log.info(message)
        await ctx.send(message)

    @convocation_setup.command(name="role")
    async def convocation_setup_role(self, ctx, role: discord.Role):
        """Setup the convocation role."""
        await self.config.guild(ctx.guild).convocation_role.set(role.id)

        message = "Convocation role configured on {}".format(role.mention)
        log.info(message)
        await ctx.send(message)
    
    @convocation_setup.command(name="staff")
    async def convocation_setup_staff_role(self, ctx, role: discord.Role):
        """Setup the staff role."""
        await self.config.guild(ctx.guild).staff_role.set(role.id)

        message = "Staff role configured on {}".format(role.mention)
        log.info(message)
        await ctx.send(message)
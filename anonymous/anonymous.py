import discord
import json
import logging
import os
import random

from datetime import date, datetime, timedelta
from redbot.core import Config, checks, commands, data_manager
from redbot.core.utils.embed import randomize_colour
from redbot.core.utils.tunnel import Tunnel

log = logging.getLogger("red.anonymous")

class Anonymous(commands.Cog):

    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20210628)
        default_guild = {
            "mapped_channels": dict()
        }
        self.config.register_guild(**default_guild)

        self.json_file = str(data_manager.cog_data_path()) + "/{}/users.json".format(self.__class__.__name__)

    def cog_unload(self):
        log.info("Unload")

    @commands.group(name = "anonyme")
    async def anonymous(self, ctx):
        """
        Systeme d'envoi de message anonyme.
        Voir les sous-commandes
        """
        pass

    @anonymous.command(name = "message")
    @commands.dm_only()
    async def a_message(self, ctx, salon: str, *, message: str):
        """
        Envoie un message anonyme dans le salon spécifié.
        Faire `!anonyme salons` pour avoir la liste des salons disponibles.
        Faire `!anonyme nouveau` pour obtenir un nouvel identifiant. Peut-être utilise si vous ne souhaitez pas garder le même identifiant pour une nouvelle discussion.
        Attention, l'identifiant permet au staff de retrouver la personne qui envoie un message, tout abus sera passible de sanctions.
        """
        users = None
        user_tag = None
        with open(self.json_file, 'r') as json_file:
            users = json.load(json_file)
        
        if users is not None:
            if str(ctx.author.id) in users:
                user_tag = users[str(ctx.author.id)]
            else:
                tag = random.randint(1, 9999)
                while tag in users.values():
                    tag = random.randint(1, 9999)
                user_tag = str(tag).zfill(4)
                users[str(ctx.author.id)] = user_tag
                with open(self.json_file, 'w') as json_file:
                    json.dump(users, json_file)

        if user_tag is not None:
            for guild in self.bot.guilds:
                mapped_channels_ids = await self.config.guild(guild).mapped_channels()
                if salon in mapped_channels_ids:
                    files = await Tunnel.files_from_attach(ctx.message)

                    em = discord.Embed(description = message, color=0x66deba)
                    em.set_author(name = f"Anonyme#{user_tag}")

                    channel = guild.get_channel(mapped_channels_ids[salon])

                    await Tunnel.message_forwarder(destination=channel, embed=em, files=files)
                    await ctx.message.add_reaction('✅')
                else:
                    await ctx.send_help(self.a_message)

    @anonymous.command(name = "salons")
    @commands.dm_only()
    async def a_channels(self, ctx):
        """
        Affiche la liste des salons disponibles
        """
        guilds = self.bot.guilds
        for guild in guilds:
            mapped_channels_ids = await self.config.guild(guild).mapped_channels()
            mapped_channels = list(map(lambda c: str(c[0]), mapped_channels_ids.items()))

            embed = discord.Embed(title = "Salons disponibles", description = "- " + "\n- ".join(mapped_channels), color = 0xde2100)

            await ctx.send(embed = embed)
    
    @anonymous.command(name = "nouveau")
    @commands.dm_only()
    async def a_reset(self, ctx):
        """
        Permet d'obtenir un nouvel identifiant.
        Tant que cette commande n'est pas utilisée, votre identifiant sera toujours le même,
        ce qui peut permettre aux autres personnes de suivre la discussion.
        """
        users = None
        with open(self.json_file, 'r') as json_file:
            users = json.load(json_file)
        
        if users is not None:
            tag = random.randint(1, 9999)
            while tag in users.values():
                tag = random.randint(1, 9999)
            user_tag = str(tag).zfill(4)
            users[str(ctx.author.id)] = user_tag
            with open(self.json_file, 'w') as json_file:
                json.dump(users, json_file)
        
        await ctx.message.add_reaction('✅')
    
    @anonymous.command(name = "utilisateur")
    @commands.guild_only()
    @checks.admin_or_permissions(administrator = True)
    async def a_user(self, ctx, tag: str):
        """
        Retrouve un utilisateur à partir de son tag
        """
        with open(self.json_file, 'r') as json_file:
            users = json.load(json_file)
            user = list(users.keys())[list(users.values()).index(tag)]
            print(user)
            member = ctx.guild.get_member(int(user))
            await ctx.send(f"Il s'agit de {member.mention}")

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def anonymous_setup(self, ctx):
        """Setup anonymous questions system"""
        pass

    @anonymous_setup.group(name = "channels")
    async def as_channels(self, ctx):
        """Add or remove configured channels"""
        pass

    @as_channels.command(name = "add")
    async def as_channels_add(self, ctx, channel: discord.TextChannel, alias: str):
        """Add a channel to allowed channels"""
        mapped_channels = await self.config.guild(ctx.guild).mapped_channels()
        mapped_channels[alias] = channel.id
        await self.config.guild(ctx.guild).mapped_channels.set(mapped_channels)
        await ctx.message.add_reaction('✅')
    
    @as_channels.command(name = "remove")
    async def as_channels_remove(self, ctx, alias: str):
        """Remove a channel to allowed channels"""
        mapped_channels = await self.config.guild(ctx.guild).mapped_channels()
        del mapped_channels[alias]
        await self.config.guild(ctx.guild).mapped_channels.set(mapped_channels)
        await ctx.message.add_reaction('✅')

    @anonymous_setup.command(name = "display")
    async def as_display(self, ctx):
        """Display configuration"""
        mapped_channels_ids = await self.config.guild(ctx.guild).mapped_channels()
        mapped_channels = list(map(lambda c: str(c[0]) + " → " + str(c[1]) if ctx.guild.get_channel(c[1]) is None else str(c[0]) + " → " + ctx.guild.get_channel(c[1]).mention, mapped_channels_ids.items()))

        embed = discord.Embed(title = "Salons mappés", description = "- " + "\n- ".join(mapped_channels), color = 0xde2100)
        embed.set_footer(text = "Total: {}".format(len(mapped_channels)))

        await ctx.send(embed = embed)

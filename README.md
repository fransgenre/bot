# Fransgenre Bot
Dépôt contenant les cogs pour le bot Fransgenre.

## Installation et configuration du bot
Utilisation de l'image docker `phasecorex/red-discordbot:noaudio`

Commande de création du conteneur:
```bash
docker run -d --restart=always --name <nom du conteneur> -v <dossier de persistence sur le host>:/data -v <dossier contenant les cogs>:/cogs -e TOKEN=<token> -e PREFIX=<prefix> -e TZ=Europe/Paris -e PUID=<id du user du host à utiliser> -e PGID=<id du groupe du host à utiliser> -e EXTRA_ARGS="--co-owner <liste des id des co-owners>" phasecorex/red-discordbot:noaudio
```

Après démarrage du bot, sur discord, exécuter:  
`!addpath /cogs`  
pour ajouter le dossier où sont situés les cogs.

Pour charger/décharger les cogs:  
`!load <cog>`  
ou  
`!unload <cog>`

## Cogs présent dans le dépôt

### clear
Permet de supprimer des messages.

### purge
Gestion des membres inactif⋅ve⋅s

### report
Permet d'envoyer un rapport à la modération.

### validation
Gestion des nouveau membres sur le serveur.

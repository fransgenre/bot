import discord
from redbot.core import Config, checks, commands
from redbot.core.utils.tunnel import Tunnel
from datetime import datetime

class Report(commands.Cog):

    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20210211)
        default_guild = {
            "report_channel": None,
            "next_ticket": 1
        }
        self.config.register_guild(**default_guild)

    @commands.command()
    async def report(self, ctx, *, report: str = ""):
        """Send a ticket to moderation"""
        if (type(ctx.channel) is discord.DMChannel):
            if report == "":
                await ctx.message.add_reaction('❌')
                await ctx.send("Le message de report est vide")
                return
            guild = self.find_guild(ctx.author)

            now = datetime.now().strftime("%d/%m/%Y à %H:%M:%S")
            author = guild.get_member(ctx.author.id)

            ticket_number = await self.config.guild(guild).next_ticket()
            await self.config.guild(guild).next_ticket.set(ticket_number + 1)
            
            title = "Rapport de {} {} envoyé le {}".format(author, f"({author.nick})" if author.nick else "", now)

            files = await Tunnel.files_from_attach(ctx.message)

            em = discord.Embed(description=report, color=0xdd8be8)
            em.set_author(name=title, icon_url=author.avatar_url)
            em.set_footer(text="Rapport #{}".format(ticket_number))

            channel_id = await self.config.guild(guild).report_channel()
            channel = guild.get_channel(channel_id)

            await Tunnel.message_forwarder(destination=channel, embed=em, files=files)

            await ctx.message.add_reaction('✅')
            await ctx.send("Votre rapport a bien été envoyé")

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def report_setup(self, ctx):
        """Setup report"""
        pass

    @report_setup.command(name = "channel")
    async def report_setup_channel(self, ctx, channel: discord.TextChannel):
        """Setup the report channel."""
        await self.config.guild(ctx.guild).report_channel.set(channel.id)

        message = "Report channel configured on {}".format(channel.mention)
        await ctx.send(message)

    def find_guild(self, user):
        for guild in self.bot.guilds:
            x = guild.get_member(user.id)
            if x is not None:
                return guild

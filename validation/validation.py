import asyncio
import datetime as dt
import json

import discord
from discord.ext import tasks
import logging
from redbot.core import Config, checks, commands, utils


log = logging.getLogger("red.validation")

class Abort(Exception):
    pass


class Validation(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20181201)
        default_guild = {
            "entrance_channel": None,
            "archive_channel": None,
            "welcome_channel": None,
            "mod_channel": None,
            "alert_channel": None,
            "message_validation": None,
            "message_welcome": None,
            "admin_role": None,
            "member_role": None,
            "invited_role": None,
            "entrance_category" : None,
            "optional_roles": {},
            "time_before_kick": {"days": 1}
        }
        self.config.register_guild(**default_guild)
        
        self.kick_daemon.start()

    def cog_unload(self):
        log.info("Unload")
        self.kick_daemon.cancel()

    @tasks.loop(minutes=10.0)
    async def kick_daemon(self):
        logging = True
        if logging: log.info("Checking members to kick")
        
        guilds = self.bot.guilds
        for guild in guilds:
            if logging: log.info(f"Guild {guild.name}")
            time_limit = dt.datetime.utcnow() - dt.timedelta(**(await self.config.guild(guild).time_before_kick()))
            if logging: log.info(f"Time Limit {time_limit}")
            
            entrance_category_id = await self.config.guild(guild).entrance_category()
            if not entrance_category_id and entrance_category_id <= 0:
                if logging: log.info(f"Guild {guild.name} not configured")
                continue
            entrance_category = self.bot.get_channel(entrance_category_id)
            for channel in entrance_category.text_channels:
                if not channel.name.startswith("accueil"):
                    if logging: log.info(f"Skip {channel.name}")
                    continue
                
                if logging: log.info(f"Check Channel {channel.name}")
                members = channel.members
                for member in members:
                    if isinstance(member, discord.User):
                        log.info(f"Discord py bug {member.name} is not a member")
                        continue
                    
                    if logging: log.info(f"Check Member {member.name}")
                    # On ignore les membres
                    if await self._is_mod(member):
                        if logging: log.info(f"Check Member {member.name}: Mod")
                        continue

                    if logging: log.info(f"Check Member {member.name}: Not mod")
                    # Si il est arrivé depuis - 24h alors on kick pas
                    if logging: log.info(f"Checking join {member}")
                    if member.joined_at is not None and member.joined_at > time_limit:
                        if logging: log.info(f"Check Member {member.name}: Joined after time limit")
                        continue
                    if logging: log.info(f"Check Member {member.name}: Joined before time limit")
                    # Si il a posté depuis -24h alors on kick pas
                    if logging: log.info(f"Checking active {member}")
                    active_bool = False
                    messages = await channel.history(after=time_limit).flatten()
                    for message in messages:
                        if message.author == member:
                            active_bool = True
                            break

                    if active_bool:
                        if logging: log.info("Skipping member {} because they have spoken in the last 24 hours".format(member.name))
                        continue
                    if logging: log.info(f"Check Member {member.name}: Inactive")

                    # Sinon, ben on le kick
                    # Mais first on clean les messages qui se rapportes a lui
                    if logging: log.info(" - Auto Kick: Membre {} vient d'etre kick pour non validation".format(member.name))

                    try:
                        await self._close_channel(channel, "Auto Kick", member, self.bot.user)
                        await member.kick(reason="Kick automatique pour non validation")
                    except:
                        log.info("Failed to kick member {}".format(member.name))
                        continue

                    mod_message = "{} kick automatique pour non validation".format(member)
                    await self.bot.get_channel(await self.config.guild(guild).mod_channel()).send(mod_message)

    @kick_daemon.before_loop
    async def before_kick_daemon(self):
        log.info("Waiting for bot to be ready")
        await self.bot.wait_until_ready()
        log.info("Bot is ready, starting kick daemon")

################################################ LISTENERS

    @commands.Cog.listener()
    async def on_member_join(self, member):
        log.info("Membre {} vient de rejoindre".format(member.name))

        # if (member.id % 4) != 0 and member.id != 354047584039337984:
        #     keywords = {"SERVER": member.guild, "MEMBER": member.mention}
        #     message = (await self.config.guild(member.guild).message_validation()).format(**keywords)
        #     message += " ({})".format((dt.datetime.now() - member.created_at).days)
        #     await self.bot.get_channel(await self.config.guild(member.guild).entrance_channel()).send(message)
        # else:
        try:
            guild = member.guild
            
            entrance_category = self.bot.get_channel(await self.config.guild(guild).entrance_category())
            
            # On cree le channel d'accueil
            channel_name = f"accueil-{member.name}"
            channel_topic = f"{member.id}"
            accueil_channel = await entrance_category.create_text_channel(channel_name, end=True, slowmode_delay=10, topic=channel_topic)
            await accueil_channel.set_permissions(member, read_messages=True, send_messages=True)
            
            # On poste le message de bienvenue
            keywords = {"SERVER": member.guild, "MEMBER": member.mention}
            message = (await self.config.guild(member.guild).message_validation()).format(**keywords)
            message += " ({})".format((dt.datetime.now() - member.created_at).days)
            
            await accueil_channel.send(message)
            
            entrance_channel = guild.get_channel(await self.config.guild(member.guild).entrance_channel())
            await entrance_channel.set_permissions(member, view_channel = False, read_messages=False, send_messages=False)
        except:
            alert_channel = await self.bot.get_channel(await self.config.guild(member.guild).alert_channel())
            await alert_channel.send("Erreur lors de la creation du salon d'accueil pour le membre {member.mention} ({member.id})")

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        mod_message = f"Membre {member.mention} {member.name} {member.id} vient de quitter "
        log.info(mod_message)
        await self.bot.get_channel(await self.config.guild(member.guild).mod_channel()).send(mod_message)
        
        channel = await self._get_member_channel(member.guild, member)
        
        await self._close_channel(channel, "Kick/Leave", member, self.bot.user)

    @commands.Cog.listener()
    async def on_member_ban(self, guild, user):
        log.info("Membre {} vient d'etre banni".format(user.name))
        
        channel = await self._get_member_channel(guild, user)
        await self._close_channel(channel, "Ban", user, self.bot.user)


################################################ COMMANDS

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def qval(self, ctx, member: discord.Member, *roles):
        """Quietly accept a member in entrance channel."""
        member_role = ctx.guild.get_role(await self.config.guild(ctx.guild).member_role())
        
        await self._validate_member(ctx, member, member_role, roles)

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def ival(self, ctx, member: discord.Member, *roles):
        """Quietly accept an invited member in entrance channel."""
        invited_role = ctx.guild.get_role(await self.config.guild(ctx.guild).invited_role())
        
        await self._validate_member(ctx, member, invited_role, roles)

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def aval(self, ctx, member: discord.Member, *roles):
        """Accept a member in entrance channel."""
        member_role = ctx.guild.get_role(await self.config.guild(ctx.guild).member_role())

        await self._validate_member(ctx, member, member_role, roles)

        keywords = {"SERVER": ctx.guild, "MEMBER": member.mention}
        message = (await self.config.guild(ctx.guild).message_welcome()).format(**keywords)
        await self.bot.get_channel(await self.config.guild(ctx.guild).welcome_channel()).send(message)

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def get_channel(self, ctx, member: discord.Member):
        """Own a channel."""
        channel = await self._get_member_channel(ctx.guild, member)
        await ctx.send(f"Found accueil channel for {member}: {channel.name}")

    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def aban(self, ctx, member: discord.Member, reason: str = None):
        """Ban a member in entrance channel."""
        if not await self._is_valid_action(ctx, member):
            return

        log.info("Banning {}".format(member.name))

        # Let's ban the user
        await ctx.guild.ban(member, reason=reason)

        mod_message = "{} vient d'être banni par {} - Raison: {}".format(
            member, ctx.message.author, reason)
        await self.bot.get_channel(await self.config.guild(ctx.guild).mod_channel()).send(mod_message)

        await self._close_channel(ctx.channel, "Ban", member, ctx.message.author)

################################################ CONFIGURATION

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def val_setup(self, ctx):
        pass

    @val_setup.group()
    async def channel(self, ctx):
        pass

    @val_setup.group()
    async def message(self, ctx):
        pass

    @val_setup.group()
    async def role(self, ctx):
        pass
    
    @val_setup.command(name="show")
    async def val_setup_show(self, ctx):
        """Show the configuration."""
        config = await self.config.guild(ctx.guild).get_raw()
        message = ""
        for name, value in config.items():
            if "channel" in name and type(value) is int:
                message_temp = f"**{name}** : {ctx.guild.get_channel(value).mention}\n"
            elif "role" in name and type(value) is int:
                message_temp = f"**{name}** : {ctx.guild.get_role(value).mention}\n"
            elif type(value) is dict:
                value_str = str(json.dumps(value, indent=4, sort_keys=True))
                message_temp = f"**{name}** : \n```{value_str}```\n"
            elif type(value) is str:
                message_temp = f"**{name}** : \n```{value}```\n"
            else:
                message_temp = f"**{name}** : ({type(value)})\n```{value}```\n"

            if len(message) + len(message_temp) >= 4000:
                e = discord.Embed(colour=discord.Colour.red(), title="Configuration de Validation", description=message)
                await ctx.send(embed=e)
                message = ""
            if len(message_temp) >= 4096:
                message_temp = message_temp[:4080] + " ...```"
            message += message_temp
        e = discord.Embed(colour=discord.Colour.red(), title="Configuration de Validation", description=message)
        await ctx.send(embed=e)

    
    @channel.command(name="alert")
    async def val_setup_channel_alert(self, ctx, channel: discord.TextChannel):
        """Setup the alert channel."""
        await self.config.guild(ctx.guild).alert_channel.set(channel.id)

        message = "Alert channel configured on {}".format(channel.mention)
        await ctx.send(message)

    @channel.command(name="archive")
    async def val_setup_channel_archive(self, ctx, channel: discord.TextChannel):
        """Setup the archive channel."""
        await self.config.guild(ctx.guild).archive_channel.set(channel.id)

        message = "Archive channel configured on {}".format(channel.mention)
        await ctx.send(message)

    @channel.command(name="general", aliases=['welcome'])
    async def val_setup_channel_welcome(self, ctx, channel: discord.TextChannel):
        """Setup the welcome channel."""
        await self.config.guild(ctx.guild).welcome_channel.set(channel.id)

        message = "Welcome channel configured on {}".format(channel.mention)
        await ctx.send(message)

    @channel.command(name="moderation", aliases=['mod'])
    async def val_setup_channel_mod(self, ctx, channel: discord.TextChannel):
        """Setup the mod channel."""
        await self.config.guild(ctx.guild).mod_channel.set(channel.id)

        message = "Moderation channel configured on {}".format(channel.mention)
        await ctx.send(message)
    
    @channel.command(name="entrance")
    async def val_setup_channel_entrance(self, ctx, channel: discord.TextChannel):
        """Setup the entrance channel."""
        await self.config.guild(ctx.guild).entrance_channel.set(channel.id)

        message = "Entrance channel configured on {}".format(channel.mention)
        await ctx.send(message)

    
    @channel.command(name="category")
    async def val_setup_channel_category(self, ctx, category: discord.CategoryChannel):
        """Setup the entrance category."""
        await self.config.guild(ctx.guild).entrance_category.set(category.id)

        message = "Entrance category configured on {}".format(category.mention)
        await ctx.send(message)

    @message.command(name="validation")
    async def val_setup_message_validation(self, ctx):
        """Setup the validation message."""

        def check(m):
            return m.author == ctx.author and m.channel == ctx.channel

        instructions = "You have 2 minutes to post in the next message the content of the validation message.\n" \
                       + "You can enter `{SERVER}` for server name, `{MEMBER}` for member name, or `abort` to abort"
        instructions_msg = await ctx.send(instructions)
        try:
            message = await self.bot.wait_for('message', check=check, timeout=120)
            if message.content.strip() == "abort":
                raise Abort()
        except asyncio.TimeoutError:
            await ctx.send("{} Too late, please be quicker next time".format(ctx.author.mention))
        except Abort:
            await ctx.send("Edit aborted".format(ctx.author.mention))
        else:
            await self.config.guild(ctx.guild).message_validation.set(message.content)
            confirmation = "Validation message configured on ```{}```".format(
                message.content)
            await ctx.send(confirmation)
            await message.add_reaction('✅')
        finally:
            await instructions_msg.delete()

    @message.command(name="welcome")
    async def val_setup_message_welcome(self, ctx):
        """Setup the welcome message."""

        def check(m):
            return m.author == ctx.author and m.channel == ctx.channel

        instructions = "You have 2 minutes to post in the next message the content of the welcome message"
        instructions_msg = await ctx.send(instructions)
        try:
            message = await self.bot.wait_for('message', check=check, timeout=120)
            if message.content.strip() == "abort":
                raise Abort()
        except asyncio.TimeoutError:
            await ctx.send("{} Too late, please be quicker next time".format(ctx.author.mention))
        except Abort:
            await ctx.send("Edit aborted".format(ctx.author.mention))
        else:
            await self.config.guild(ctx.guild).message_welcome.set(message.content)
            confirmation = "Welcome message configured on ```{}```".format(
                message.content)
            await ctx.send(confirmation)
            await message.add_reaction('✅')
        finally:
            await instructions_msg.delete()

    @role.command(name="member")
    async def val_member_role_member(self, ctx, role: discord.Role):
        """Setup the member role after validation."""
        await self.config.guild(ctx.guild).member_role.set(role.id)

        message = "Member role configured on {}".format(role)
        await ctx.send(message)

    @role.command(name="invited")
    async def val_member_role_invited(self, ctx, role: discord.Role):
        """Setup the invited role after validation."""
        await self.config.guild(ctx.guild).invited_role.set(role.id)

        message = "Invited role configured on {}".format(role)
        await ctx.send(message)

    @role.command(name="admin")
    async def val_member_role_admin(self, ctx, role: discord.Role):
        """Setup the admin role."""
        await self.config.guild(ctx.guild).admin_role.set(role.id)

        await ctx.send("Admin role configured on {}".format(role))

    @role.group(name="optional")
    async def val_member_role_optional(self, ctx):
        pass

    @val_member_role_optional.command(name="set")
    async def val_member_role_optional_set(self, ctx, role: discord.Role, *tags):
        for tag in tags:
            await self.config.guild(ctx.guild).set_raw('optional_roles', tag, value=role.id)
        await ctx.send("Mapped {} role on tags {}".format(role, " ".join(tags)))

    @val_member_role_optional.command(name="del")
    async def val_member_role_optional_del(self, ctx, *tags):
        for tag in tags:
            await self.config.guild(ctx.guild).clear_raw('optional_roles', tag)
        await ctx.send("Del tags: {}".format(" ".join(tags)))

    @val_member_role_optional.command(name="get")
    async def val_member_role_optional_get(self, ctx, *tags):
        data = await self.config.guild(ctx.guild).get_raw('optional_roles', *tags)
        data_str = str(json.dumps(data, indent=4, sort_keys=True))
        for role in ctx.guild.roles:
            data_str = data_str.replace(str(role.id), role.name)
        message = '```json\n{}```'.format(data_str)
        await ctx.send(message)

############################ HELPERS

    async def _get_member_channel(self, guild, member):
        for channel in guild.channels:
            if channel.name.startswith("accueil") and channel.topic == f"{member.id}":
                return channel
        return await self.bot.get_channel(await self.config.guild(guild).entrance_channel())

    async def _validate_member(self, ctx, member: discord.Member, validation_role, roles):
        if not await self._is_valid_action(ctx, member):
            return

        log.info("Validating {}".format(member.name))

        roles_to_setup = [validation_role]
        list_roles = await self.config.guild(ctx.guild).optional_roles()
        for role_tag in roles:
            try:
                role = ctx.guild.get_role(list_roles[role_tag])
                roles_to_setup.append(role)
            except:
                continue

        # Ajoutons les droits au nouveau
        for role in roles_to_setup:
            try:
                # log.info("Validating {}, add role {}".format(member.name, role.name))
                await member.add_roles(role, reason="Ajout d'un nouveau")
            except:
                log.info(f"Error Trying to add a role, {role}")
                continue

        mod_message = f"{member} vient d'être ajouté au serveur par {ctx.message.author}"
        await self.bot.get_channel(await self.config.guild(ctx.guild).mod_channel()).send(mod_message)

        await self._close_channel(ctx.channel, "Validation", member, ctx.message.author)
        
        entrance_channel = ctx.guild.get_channel(await self.config.guild(ctx.guild).entrance_channel())
        await entrance_channel.set_permissions(member, overwrite=None)
        # log.info(f"Remove overwrites {member}")

    
    async def _close_channel(self, channel, reason, member, staff):
        log.info(f"Close Channel {channel}")
        await self._backup_msg(channel, reason, member, staff)
        if channel.topic == f"{member.id}":
            # log.info(f"Delete Channel {channel}")
            await channel.delete()
        else:
            # log.info(f"Delete messages in Channel {channel}")
            messages = await self._old_find_related_msg(channel, member)
            await channel.delete_messages(messages)

    async def _is_valid_action(self, ctx, member):
        if await self._is_mod(member) or member not in ctx.channel.members or not ctx.channel.name.startswith("accueil"):
            message_usr = '{} : Action non valide, le membre doit faire parti du salon et ne pas etre staff'.format(
                ctx.message.author.mention)
            await ctx.send(message_usr, delete_after=2)
            await ctx.message.delete()
            return False
        return True

    async def _old_find_related_msg(self, channel, member):
        related_msg = []
        async for msg in channel.history(oldest_first=True):
            if msg.author == member:
                related_msg.append(msg)
            elif member.id in msg.raw_mentions:
                related_msg.append(msg)
        return related_msg

    async def _find_related_msg(self, channel):
        related_msg = await channel.history(oldest_first=True).flatten()
        return related_msg

    async def _backup_msg(self, channel, action: str, member: discord.Member, staff):
        messages = []
        if channel.topic == f"{member.id}":
            messages = await self._find_related_msg(channel)
        else:
            messages = await self._old_find_related_msg(channel,member)

        if len(messages) <= 0:
            return

        # log.info("Backup messages")
        message_to_post = '# DEBUT - Action: ' + action + ' - Membre: {} ({})'.format(member, member.id) \
                          + ' - Modérateur: {} ({})\n\n'.format(staff, staff.id)

        for msg in messages:
            if await self._is_mod(msg.author, True):
                tmp = '[' + str(msg.author)[:-5] + '] :\n'
            else:
                tmp = '<' + str(msg.author)[:-5] + '> :\n'
            tmp += msg.clean_content + '\n\n'

            message_to_post += tmp
        message_to_post += '# FIN'
        messages_to_post = utils.chat_formatting.pagify(message_to_post, "\n<", shorten_by=10)
        # log.info("Post message")
        for message in messages_to_post:
            message = f"```md\n{message}```"
            await self.bot.get_channel(await self.config.guild(channel.guild).archive_channel()).send(message)

    async def _is_mod(self, member: discord.Member, logging = False):
        try:
            if member.bot:
                return True
            elif member.guild_permissions.administrator:
                return True
            elif member.id == member.guild.owner_id:
                return True
            else:
                admin_role = member.guild.get_role(await self.config.guild(member.guild).admin_role())
                for role in member.roles:
                    if role.id == admin_role.id:
                        return True
            return False
        except AttributeError:
            return False

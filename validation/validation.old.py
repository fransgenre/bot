import asyncio
import datetime as dt
import json

import discord
from discord.ext import tasks
import logging
from redbot.core import Config, checks, commands, utils


log = logging.getLogger("red.validation")

class Abort(Exception):
    pass


class Validation(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20181201)
        default_guild = {
            "entrance_channel": None,
            "archive_channel": None,
            "welcome_channel": None,
            "mod_channel": None,
            "message_validation": None,
            "message_welcome": None,
            "admin_role": None,
            "setup_role": None,
            "optional_roles": {},
            "time_before_kick": {"days": 1},
            "optionnal_roles_description": {}
        }
        self.config.register_guild(**default_guild)
        
        self.kick_daemon.start()

    def cog_unload(self):
        log.info("Unload")
        self.kick_daemon.cancel()

    @tasks.loop(minutes=10.0)
    async def kick_daemon(self):
        log.info("Checking members to kick")

        guilds = self.bot.guilds
        for guild in guilds:

            time_limit = dt.datetime.utcnow() - dt.timedelta(**(await self.config.guild(guild).time_before_kick()))
            channel = self.bot.get_channel(await self.config.guild(guild).entrance_channel())
            members = channel.members
            for member in members:
                # On ignore les membres
                if await self._is_mod(member):
                    continue
                # Si il est arrivé depuis - 24h alors on kick pas
                if member.joined_at > time_limit:
                    continue
                # Si il a posté depuis -24h alors on kick pas
                active_bool = False
                messages = await channel.history(after=time_limit).flatten()
                for message in messages:
                    if message.author == member:
                        active_bool = True
                        break
                if active_bool:
                    log.info("Skipping member {} because they have spoken in the last 24 hours".format(member.name))
                    continue

                # Sinon, ben on le kick
                # Mais first on clean les messages qui se rapportes a lui
                log.info(" - Auto Kick: Membre {} vient d'etre kick pour non validation".format(member.name))

                msg = await self._find_related_msg(member)
                await channel.delete_messages(msg)
                try:
                    await member.kick(reason="Kick automatique pour non validation")
                except:
                    log.info("Failed to kick member {}".format(member.name))
                    continue

                mod_message = "{} kick automatique pour non validation".format(member)
                await self.bot.get_channel(await self.config.guild(guild).mod_channel()).send(mod_message)

    @kick_daemon.before_loop
    async def before_kick_daemon(self):
        log.info("Waiting for bot to be ready")
        await self.bot.wait_until_ready()
        log.info("Bot is ready, starting kick daemon")

    @commands.Cog.listener()
    async def on_member_join(self, member):
        log.info("Membre {} vient de rejoindre".format(member.name))

        keywords = {"SERVER": member.guild, "MEMBER": member.mention}
        message = (await self.config.guild(member.guild).message_validation()).format(**keywords)
        message += " ({})".format((dt.datetime.now() - member.created_at).days)
        await self.bot.get_channel(await self.config.guild(member.guild).entrance_channel()).send(message)

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        log.info("Membre {} vient de quitter type ({})".format(member.name, type(member)))
        await self._remove_messages_from_member(member, self.bot.user, "Kick/Leave")

    @commands.Cog.listener()
    async def on_member_ban(self, member):
        log.info("Membre {} vient d'etre banni".format(member.name))

        await self._remove_messages_from_member(member, self.bot.user, "Ban")

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def val_setup(self, ctx):
        pass

    @val_setup.group()
    async def channel(self, ctx):
        pass

    @val_setup.group()
    async def message(self, ctx):
        pass

    @val_setup.group()
    async def role(self, ctx):
        pass

    @channel.command(name="entrance", aliases=['entry'])
    async def val_setup_channel_entry(self, ctx, channel: discord.TextChannel):
        """Setup the entrance channel."""
        await self.config.guild(ctx.guild).entrance_channel.set(channel.id)

        message = "Entrance channel configured on {}".format(channel.mention)
        await ctx.send(message)

    @channel.command(name="archive")
    async def val_setup_channel_archive(self, ctx, channel: discord.TextChannel):
        """Setup the archive channel."""
        await self.config.guild(ctx.guild).archive_channel.set(channel.id)

        message = "Archive channel configured on {}".format(channel.mention)
        await ctx.send(message)

    @channel.command(name="general", aliases=['welcome'])
    async def val_setup_channel_welcome(self, ctx, channel: discord.TextChannel):
        """Setup the welcome channel."""
        await self.config.guild(ctx.guild).welcome_channel.set(channel.id)

        message = "Welcome channel configured on {}".format(channel.mention)
        await ctx.send(message)

    @channel.command(name="moderation", aliases=['mod'])
    async def val_setup_channel_mod(self, ctx, channel: discord.TextChannel):
        """Setup the mod channel."""
        await self.config.guild(ctx.guild).mod_channel.set(channel.id)

        message = "Moderation channel configured on {}".format(channel.mention)
        await ctx.send(message)

    @message.command(name="validation")
    async def val_setup_message_validation(self, ctx):
        """Setup the validation message."""

        def check(m):
            return m.author == ctx.author and m.channel == ctx.channel

        instructions = "You have 2 minutes to post in the next message the content of the validation message.\n" \
                       + "You can enter `{SERVER}` for server name, `{MEMBER}` for member name, or `abort` to abort"
        instructions_msg = await ctx.send(instructions)
        try:
            message = await self.bot.wait_for('message', check=check, timeout=120)
            if message.content.strip() == "abort":
                raise Abort()
        except asyncio.TimeoutError:
            await ctx.send("{} Too late, please be quicker next time".format(ctx.author.mention))
        except Abort:
            await ctx.send("Edit aborted".format(ctx.author.mention))
        else:
            await self.config.guild(ctx.guild).message_validation.set(message.content)
            confirmation = "Validation message configured on ```{}```".format(
                message.content)
            await ctx.send(confirmation)
            await message.add_reaction('✅')
        finally:
            await instructions_msg.delete()

    @message.command(name="welcome")
    async def val_setup_message_welcome(self, ctx):
        """Setup the welcome message."""

        def check(m):
            return m.author == ctx.author and m.channel == ctx.channel

        instructions = "You have 2 minutes to post in the next message the content of the welcome message"
        instructions_msg = await ctx.send(instructions)
        try:
            message = await self.bot.wait_for('message', check=check, timeout=120)
            if message.content.strip() == "abort":
                raise Abort()
        except asyncio.TimeoutError:
            await ctx.send("{} Too late, please be quicker next time".format(ctx.author.mention))
        except Abort:
            await ctx.send("Edit aborted".format(ctx.author.mention))
        else:
            await self.config.guild(ctx.guild).message_welcome.set(message.content)
            confirmation = "Welcome message configured on ```{}```".format(
                message.content)
            await ctx.send(confirmation)
            await message.add_reaction('✅')
        finally:
            await instructions_msg.delete()

    @role.command(name="setup")
    async def val_setup_role_setup(self, ctx, role: discord.Role):
        """Setup the role after validation."""
        await self.config.guild(ctx.guild).setup_role.set(role.id)

        message = "Setup role configured on {}".format(role)
        await ctx.send(message)

    @role.command(name="admin")
    async def val_setup_role_admin(self, ctx, role: discord.Role):
        """Setup the admin role."""
        await self.config.guild(ctx.guild).admin_role.set(role.id)

        await ctx.send("Admin role configured on {}".format(role))

    @role.group(name="optional")
    async def val_setup_role_optional(self, ctx):
        pass

    @val_setup_role_optional.command(name="set")
    async def val_setup_role_optional_set(self, ctx, role: discord.Role, *tags):
        for tag in tags:
            await self.config.guild(ctx.guild).set_raw('optional_roles', tag, value=role.id)
        await ctx.send("Mapped {} role on tags {}".format(role, " ".join(tags)))

    @val_setup_role_optional.command(name="del")
    async def val_setup_role_optional_del(self, ctx, *tags):
        for tag in tags:
            await self.config.guild(ctx.guild).clear_raw('optional_roles', tag)
        await ctx.send("Del tags: {}".format(" ".join(tags)))

    @val_setup_role_optional.command(name="get")
    async def val_setup_role_optional_get(self, ctx, *tags):
        data = await self.config.guild(ctx.guild).get_raw('optional_roles', *tags)
        data_str = str(json.dumps(data, indent=4, sort_keys=True))
        for role in ctx.guild.roles:
            data_str = data_str.replace(str(role.id), role.name)
        message = '```json\n{}```'.format(data_str)
        await ctx.send(message)


    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def qval(self, ctx, member: discord.Member, *roles):
        """Quietly accept a member in entrance channel."""

        await self._validate_member(ctx, member, roles)


    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def aval(self, ctx, member: discord.Member, *roles):
        """Accept a member in entrance channel."""

        await self._validate_member(ctx, member, roles)

        keywords = {"SERVER": ctx.guild, "MEMBER": member.mention}
        message = (await self.config.guild(ctx.guild).message_welcome()).format(**keywords)
        await self.bot.get_channel(await self.config.guild(ctx.guild).welcome_channel()).send(message)



    @commands.command()
    @checks.mod_or_permissions(administrator=True)
    async def aban(self, ctx, member: discord.Member, reason: str = None):
        """Ban a member in entrance channel."""
        if await self._is_mod(member):
            message_usr = '{} : Action non valide (membre du staff)'.format(
                ctx.message.author.mention)
            await ctx.send(message_usr, delete_after=2)
            await ctx.message.delete()
            return
        log.info("Banning {}".format(member.name))

        # Let's ban the user
        await ctx.guild.ban(member, reason=reason)

        mod_message = "{} vient d'être banni par {} - Raison: {}".format(
            member, ctx.message.author, reason)
        await self.bot.get_channel(await self.config.guild(ctx.guild).mod_channel()).send(mod_message)

        await self._remove_messages_from_member(member, ctx.message.author, "Ban")

    @commands.command(name="clean_accueil")
    @checks.mod_or_permissions(administrator=True)
    async def clean_accueil(self, ctx):
        """Clean entrance channel."""
        
        related_msg = []
        guilds = self.bot.guilds

        for guild in guilds:
            entrance_channel = self.bot.get_channel(await self.config.guild(guild).entrance_channel())
            async for msg in entrance_channel.history(oldest_first=True):
                for user in msg.mentions:
                    if isinstance(user,discord.User):
                        related_msg.append(msg)
                if msg.author.guild != guild:
                    related_msg.append(msg)
        
            message_usr = 'Delete {} messages in {}'.format(len(related_msg), guild.name)
            await ctx.send(message_usr, delete_after=2)

            await entrance_channel.delete_messages(related_msg)


    async def _validate_member(self, ctx, member: discord.Member, roles):
        if await self._is_mod(member):
            message_usr = '{} : Action non valide (membre du staff)'.format(
                ctx.message.author.mention)
            await ctx.send(message_usr, delete_after=2)
            await ctx.message.delete()
            return

        log.info("Validating {}".format(member.name))

        roles_to_setup = [ctx.guild.get_role(await self.config.guild(ctx.guild).setup_role())]
        list_roles = await self.config.guild(ctx.guild).optional_roles()
        for role in roles:
            try:
                roles_to_setup.append(ctx.guild.get_role(list_roles[role]))
            except:
                continue

        # Ajoutons les droits au nouveau
        for role in roles_to_setup:
            await member.add_roles(role, reason="Ajout d'un nouveau")

        mod_message = "{} vient d'être ajouté au serveur par {}".format(
            member, ctx.message.author)
        await self.bot.get_channel(await self.config.guild(ctx.guild).mod_channel()).send(mod_message)

        await self._remove_messages_from_member(member, ctx.message.author, "Validation")

    async def _remove_messages_from_member(self, member, staff, backup_reason):
        messages = await self._find_related_msg(member)

        if len(messages) <= 0:
            return

        await self._backup_msg(messages, backup_reason, member, staff)
        entrance = await self.config.guild(member.guild).entrance_channel()
        channel = self.bot.get_channel(entrance)
        await channel.delete_messages(messages)

    async def _find_related_msg(self, member):
        related_msg = []
        async for msg in self.bot.get_channel(await self.config.guild(member.guild).entrance_channel()).history(oldest_first=True):
            if msg.author == member:
                related_msg.append(msg)
            elif member.id in msg.raw_mentions:
                related_msg.append(msg)
        return related_msg

    async def _backup_msg(self, messages, action: str, member: discord.Member, staff):
        log.info("Backup messages")
        message_to_post = '# DEBUT - Action: ' + action + ' - Membre: {} ({})'.format(member, member.id) \
                          + ' - Modérateur: {} ({})\n\n'.format(staff, staff.id)

        for msg in messages:
            if await self._is_mod(msg.author, True):
                tmp = '[' + str(msg.author)[:-5] + '] :\n'
            else:
                tmp = '<' + str(msg.author)[:-5] + '> :\n'
            tmp += msg.clean_content + '\n\n'

            message_to_post += tmp
        message_to_post += '# FIN'
        messages_to_post = utils.chat_formatting.pagify(message_to_post, "\n<", shorten_by=10)
        for message in messages_to_post:
            message = f"```md\n{message}```"
            await self.bot.get_channel(await self.config.guild(member.guild).archive_channel()).send(message)

    async def _is_mod(self, member: discord.Member, logging = False):
        #if logging: log.info("\t\tIs Mod? {}".format(member.name))
        try:
            if member.bot:
                #if logging: log.info("\t\tYes it is a bot")
                return True
            else:
                #if logging: log.info("\t\tGet Admin role")
                admin_role = member.guild.get_role(await self.config.guild(member.guild).admin_role())
                #if logging: log.info("\t\tAdmin role is {}".format(admin_role))
                for role in member.roles:
                    if role.id == admin_role.id:
                        #if logging: log.info("\t\tYes it has admin role")
                        return True
            #sif logging: log.info("\t\tNo it is not a mod")
            return False
        except AttributeError:
            return False

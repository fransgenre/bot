import discord
import logging
from redbot.core import Config, checks, commands, utils

log = logging.getLogger("red.list")

class Listing(commands.Cog):
    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=20210523)
        default_guild = {
            "errors": [],
            "moderation_channel": None
        }
        self.config.register_guild(**default_guild)

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def list(self, ctx):
        pass

    
    @list.command(name="with")
    @checks.admin_or_permissions(administrator=True)
    async def list_with(self, ctx, *roles: discord.Role):
        """Liste tous les membres ayant la liste de roles donnee"""
        async with ctx.channel.typing():
            if len(roles) > 0:
                members = [m for m in ctx.guild.members if all(r in m.roles for r in roles)]
                roles_name = [f"{r.name}" for r in roles]
            else:
                members = [m for m in ctx.guild.members if len(m.roles) <= 1]
                roles_name = ["Aucun role"]

            await self._paginated_send(ctx, members, "Membres ayant les roles:\n" + ", ".join(roles_name))
            await ctx.message.add_reaction('✅')

    @list.command(name="roles")
    @checks.admin_or_permissions(administrator=True)
    async def list_roles(self, ctx):
        """Liste tous les roles du serveur"""
        async with ctx.channel.typing():
            await self._paginated_send(ctx, ctx.guild.roles, f"Roles de {ctx.guild.name}")
            await ctx.message.add_reaction('✅')

    @list.command(name="empty")
    @checks.admin_or_permissions(administrator=True)
    async def list_empty(self, ctx):
        """Liste tous les roles sans membres"""
        async with ctx.channel.typing():
            empty_roles = [r for r in ctx.guild.roles if len(r.members) == 0]
            await self._paginated_send(ctx, empty_roles, f"Roles vides de {ctx.guild.name}")
            await ctx.message.add_reaction('✅')


    @list.command(name="servers")
    @checks.admin_or_permissions(administrator=True)
    async def list_servers(self, ctx):
        """Liste tous les salons de tous les serveurs sur lesquels le bot est present"""
        async with ctx.channel.typing():
            guilds = self.bot.guilds
            for guild in guilds:
                await self._paginated_send(ctx, guild.channels, f"Channels de {guild.name}, {guild.owner}")

    # @list.command(name="server")
    # @checks.admin_or_permissions(administrator=True)
    # async def list_server(self, ctx):
    #     for guild in self.bot.guilds:
    #         if guild != ctx.guild and guild.id != 419486684267282432:
    #             await guild.leave()
    #         await ctx.message.add_reaction('✅')
        


    @list.command(name="in")
    @checks.admin_or_permissions(administrator=True)
    async def list_in(self, ctx, *channels: discord.TextChannel):
        """Liste tous les membres presents dans tous les salons donnes"""
        async with ctx.channel.typing():
            members = [m for m in ctx.guild.members if all(m in c.members for c in channels)]
            channels_name = [f"{c.name}" for c in channels]

            await self._paginated_send(ctx, members, "Membres ayant accès à:\n" + ", ".join(channels_name))
            await ctx.message.add_reaction('✅')


    @list.command(name="allowed")
    @checks.admin_or_permissions(administrator=True)
    async def list_allowed(self, ctx, member: discord.Member):
        """Liste tous les salons accessible par le membre"""
        async with ctx.channel.typing():
            channels = [c for c in ctx.guild.text_channels if member in c.members]
            
            await self._paginated_send(ctx, channels, f"Salons accessible par {member.name}\n")
            await ctx.message.add_reaction('✅')

    @list.command(name="denied")
    @checks.admin_or_permissions(administrator=True)
    async def list_denied(self, ctx, member: discord.Member):
        """Liste tous les salons interdits par le membre"""
        async with ctx.channel.typing():
            channels = [c for c in ctx.guild.text_channels if member not in c.members]
            
            await self._paginated_send(ctx, channels, f"Salons inaccessible par {member.name}\n")
            await ctx.message.add_reaction('✅')
    
    @list.command(name="errors")
    @checks.admin_or_permissions(administrator=True)
    async def list_errors(self, ctx):
        """Liste tous les membres ayant une combinaison de role interdite"""
        errors = await self.config.guild(ctx.guild).errors()
        async with ctx.channel.typing():
            for error in errors:
                error_roles = [ctx.guild.get_role(r) for r in error]
                members = [m for m in ctx.guild.members if all(r in m.roles for r in error_roles)]
                roles_name = [f"{r.name}" for r in error_roles]
                
                await self._paginated_send(ctx, members, "Membres ayant les roles:\n" + ", ".join(roles_name))
                await ctx.message.add_reaction('✅')

    @commands.Cog.listener()
    async def on_member_update(self, before: discord.Member, after: discord.Member):
        guild = after.guild
        errors = await self.config.guild(guild).errors()
        mod_channel = self.bot.get_channel(await self.config.guild(after.guild).moderation_channel())

        new_roles = [r_after for r_after in after.roles if r_after not in before.roles]

        if len(new_roles) <= 0:
            return

        for error in errors:
            error_roles = [guild.get_role(r) for r in error]
            if any (r in error_roles for r in new_roles) and all(r in after.roles for r in error_roles):
                roles_name = [f"{r.name}" for r in error_roles]
                
                await mod_channel.send(f"{after.mention} a les roles: " + ", ".join(roles_name))
            

######################################################## CONFIGURATION

    @commands.group()
    @commands.guild_only()
    @checks.admin_or_permissions(administrator=True)
    async def list_setup(self, ctx):
        """Set up listings"""
        pass


    @list_setup.command(name="moderation")
    async def list_setup_moderation(self, ctx, channel: discord.TextChannel):
        """Setup the mod channel."""
        await self.config.guild(ctx.guild).moderation_channel.set(channel.id)

        message = "Moderation channel configured on {}".format(channel.mention)
        log.info(message)
        await ctx.send(message)

    @list_setup.group(name = "error")
    async def list_setup_error(self, ctx): 
        pass

    @list_setup_error.command(name = "add")
    async def list_setup_error_add(self, ctx, *roles: discord.Role): 
        errors = await self.config.guild(ctx.guild).errors()
        errors.append([r.id for r in roles])
        await self.config.guild(ctx.guild).errors.set(errors)

        message = "Added [{}]".format(", ".join([r.name for r in roles]))
        await ctx.send(message)
        
    @list_setup_error.command(name = "clear")
    async def list_setup_error_clear(self, ctx):
        await self.config.guild(ctx.guild).errors.set([])

        message = "Cleared errors"
        await ctx.send(message)

    @list_setup_error.command(name = "del")
    async def list_setup_error_del(self, ctx, *roles: discord.Role): 
        errors = await self.config.guild(ctx.guild).errors()
        roles_id = [r.id for r in roles]
        errors.remove(roles_id)
        await self.config.guild(ctx.guild).errors.set(errors)

        message = "Removed [{}]".format(", ".join([r.name for r in roles]))
        await ctx.send(message)

    
    @list_setup_error.command(name = "list")
    async def list_setup_error_list(self, ctx): 
        errors = await self.config.guild(ctx.guild).errors()
        message = ""
        for error in errors:
            error_roles = [ctx.guild.get_role(r) for r in error]
            roles_name = [f"{r.mention} ({r.id})" for r in error_roles]
            message += "\n-" + ", ".join(roles_name)
        
        e = discord.Embed(colour=discord.Colour.red(), title="Combinaison de roles qui causent des erreurs", description=message)
        await ctx.send(embed=e)


######################################################## HELPERS

    async def _paginated_send(self, ctx, objects, title):
        objects_message = ""
        for i, object in enumerate(objects, start = 1):
            objects_message_tmp = f" - {object.mention} ({object}, id: {object.id})\n"
            if (len(objects_message) + len(objects_message_tmp)) >= 4000:
                embed = discord.Embed(title = title, description = objects_message, color = 0x00aa00)
                embed.set_footer(text = f"{i}/{len(objects)}")
                await ctx.send(embed = embed)
                objects_message = ""
            objects_message += objects_message_tmp
        if len(objects_message) > 0:
            embed = discord.Embed(title = title, description = objects_message, color = 0x00aa00)
            embed.set_footer(text = f"{i}/{len(objects)}")
            await ctx.send(embed = embed)
